package com.ua.iasa.core.set;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class ConcurrentHashSetTest {

    @Test
    void create() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();

        int actualSize = testInstance.size();
        int expectedSize = 10;
        assertThat(actualSize).isEqualTo(expectedSize);

        int actualElementsCount = testInstance.getElementsCount();
        int expectedElementsCount = 0;
        assertThat(actualElementsCount).isEqualTo(expectedElementsCount);
    }

    @Test
    void shouldMaintainOneElement() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();
        String entity = "entity";

        testInstance.add(entity);

        boolean actualExist = testInstance.exist(entity);
        assertThat(actualExist).isTrue();

        int actualElementsCount = testInstance.getElementsCount();
        int expectedElementsCount = 1;
        assertThat(actualElementsCount).isEqualTo(expectedElementsCount);
    }

    @Test
    void shouldMaintainTwoElement() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();
        String entity1 = "entity1";
        String entity2 = "entity2";

        testInstance.add(entity1);
        testInstance.add(entity2);

        assertThat(testInstance.exist(entity1)).isTrue();
        assertThat(testInstance.exist(entity2)).isTrue();

        int actualElementsCount = testInstance.getElementsCount();
        int expectedElementsCount = 2;
        assertThat(actualElementsCount).isEqualTo(expectedElementsCount);
    }

    @Test
    void shouldMaintainDuplicates() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();
        String entity = "entity";

        testInstance.add(entity);
        testInstance.add(entity);

        boolean actualExist = testInstance.exist(entity);
        assertThat(actualExist).isTrue();

        int actualElementsCount = testInstance.getElementsCount();
        int expectedElementsCount = 1;
        assertThat(actualElementsCount).isEqualTo(expectedElementsCount);
    }

    @Test
    void shouldMaintain100Elements() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();
        int entitiesCount = 100;

        List<String> data = IntStream.range(0, entitiesCount)
                .mapToObj((x) -> "entity" + x)
                .collect(Collectors.toList());

        data.forEach(testInstance::add);

        data.forEach(entity -> assertThat(testInstance.exist(entity)).isTrue());

        int actualElementsCount = testInstance.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(entitiesCount);
    }

    @Test
    void shouldMaintainElementsConcurrentlyTest() {
        for (int i = 0; i < 1000; i++) {
            shouldMaintainElementsConcurrently();
        }
    }

    private void shouldMaintainElementsConcurrently() {
        ConcurrentHashSet testInstance = new ConcurrentHashSet();
        List<AddEntityToHashSetTask> tasks = new ArrayList<>();

        int elementsCount = 2000;
        String entityPrefix = "entity";

        for (int i = 0; i < elementsCount; i += 50) {
            List<String> data = IntStream.range(i, i + 50)
                    .mapToObj((x) -> entityPrefix + x)
                    .collect(Collectors.toList());

            AddEntityToHashSetContext addEntityToHashSetContext = new AddEntityToHashSetContext(data, testInstance);
            AddEntityToHashSetTask task = new AddEntityToHashSetTask(addEntityToHashSetContext);
            tasks.add(task);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(8);
        tasks.forEach(executorService::submit);

        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(50, TimeUnit.MINUTES)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }

        int actualElementsCount = testInstance.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(elementsCount);

        List<String> data = IntStream.range(0, elementsCount)
                .mapToObj((x) -> entityPrefix + x)
                .collect(Collectors.toList());

        data.forEach(entity -> assertThat(testInstance.exist(entity)).isTrue());
    }
}