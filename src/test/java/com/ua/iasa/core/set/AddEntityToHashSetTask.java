package com.ua.iasa.core.set;

import java.util.List;

public class AddEntityToHashSetTask implements Runnable {

    private final AddEntityToHashSetContext addEntityToHashSetContext;

    public AddEntityToHashSetTask(AddEntityToHashSetContext addEntityToHashSetContext) {
        this.addEntityToHashSetContext = addEntityToHashSetContext;
    }

    @Override
    public void run() {
        List<String> values = addEntityToHashSetContext.getValues();
        ConcurrentHashSet concurrentHashSet = addEntityToHashSetContext.getConcurrentHashSet();
        values.forEach(concurrentHashSet::add);
    }
}
