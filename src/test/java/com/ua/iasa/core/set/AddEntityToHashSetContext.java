package com.ua.iasa.core.set;

import com.ua.iasa.core.set.ConcurrentHashSet;

import java.util.List;

public class AddEntityToHashSetContext {

    private final List<String> values;
    private final ConcurrentHashSet concurrentHashSet;

    AddEntityToHashSetContext(List<String> values, ConcurrentHashSet concurrentHashSet) {
        this.values = values;
        this.concurrentHashSet = concurrentHashSet;
    }

    public List<String> getValues() {
        return values;
    }

    public ConcurrentHashSet getConcurrentHashSet() {
        return concurrentHashSet;
    }
}
