package com.ua.iasa.core.map;

import com.ua.iasa.core.map.ConcurrentMap;
import com.ua.iasa.core.map.ConcurrentMapEntry;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ConcurrentMapTest {

    private static final String KEY1 = "key1";
    private static final String KEY2 = "key2";
    private static final String VALUE1 = "value1";
    private static final String VALUE2 = "value2";

    @Test
    void shouldCreateInvertedMap() {
        int capacity = 10;
        double loadFactor = 0.9;

        ConcurrentMap<String> actual = new ConcurrentMap<>(capacity, loadFactor);

        int actualCapacity = actual.size();
        assertThat(actualCapacity).isEqualTo(capacity);

        double actualLoadFactor = actual.getLoadFactor();
        assertThat(actualLoadFactor).isEqualTo(loadFactor);
    }

    @Test
    void shouldPutOne() {
        ConcurrentMap<String> invertedMap = new ConcurrentMap<>();

        invertedMap.put(KEY1, VALUE1);

        String actual = invertedMap.get(KEY1);
        assertThat(actual).isEqualTo(VALUE1);

        int actualElementsCount = invertedMap.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(1);
    }

    @Test
    void shouldPutTwo() {
        ConcurrentMap<String> invertedMap = new ConcurrentMap<>();

        invertedMap.put(KEY1, VALUE1);
        invertedMap.put(KEY2, VALUE2);

        String actualValue1 = invertedMap.get(KEY1);
        assertThat(actualValue1).isEqualTo(VALUE1);

        String actualValue2 = invertedMap.get(KEY2);
        assertThat(actualValue2).isEqualTo(VALUE2);

        int actualElementsCount = invertedMap.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(2);
    }

    @Test
    void shouldPutSameTwice() {
        ConcurrentMap<String> invertedMap = new ConcurrentMap<>();

        invertedMap.put(KEY1, VALUE1);
        invertedMap.put(KEY1, VALUE1);

        String actualValue1 = invertedMap.get(KEY1);
        assertThat(actualValue1).isEqualTo(VALUE1);

        int actualElementsCount = invertedMap.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(1);

        Set<String> actualKeys = invertedMap.keys();
        int actualKeyCount = actualKeys.size();
        assertThat(actualKeyCount).isEqualTo(1);
        assertThat(actualKeys).contains(KEY1);

        List<ConcurrentMapEntry<String, String>> actualEntries = invertedMap.entries();
        int actualEntitiesCount = actualEntries.size();
        assertThat(actualEntitiesCount).isEqualTo(1);

        ConcurrentMapEntry<String, String> actualEntity = actualEntries.get(0);
        String actualEntityKey = actualEntity.getKey();
        assertThat(actualEntityKey).isEqualTo(KEY1);

        String actualEntityValue = actualEntity.getValue();
        assertThat(actualEntityValue).isEqualTo(VALUE1);
    }

    @Test
    void shouldUpdateValue() {
        ConcurrentMap<String> invertedMap = new ConcurrentMap<>();

        invertedMap.put(KEY1, VALUE1);
        invertedMap.put(KEY1, VALUE2);

        String actualValue1 = invertedMap.get(KEY1);
        assertThat(actualValue1).isEqualTo(VALUE2);

        int actualElementsCount = invertedMap.getElementsCount();
        assertThat(actualElementsCount).isEqualTo(1);

        Set<String> actualKeys = invertedMap.keys();
        int actualKeyCount = actualKeys.size();
        assertThat(actualKeyCount).isEqualTo(1);
        assertThat(actualKeys).contains(KEY1);

        List<ConcurrentMapEntry<String, String>> actualEntries = invertedMap.entries();
        int actualEntitiesCount = actualEntries.size();
        assertThat(actualEntitiesCount).isEqualTo(1);

        ConcurrentMapEntry<String, String> actualEntity = actualEntries.get(0);
        String actualEntityKey = actualEntity.getKey();
        assertThat(actualEntityKey).isEqualTo(KEY1);

        String actualEntityValue = actualEntity.getValue();
        assertThat(actualEntityValue).isEqualTo(VALUE2);
    }
}