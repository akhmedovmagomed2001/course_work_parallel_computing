package com.ua.iasa.core.map;

import com.ua.iasa.core.map.ConcurrentMapEntry;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ConcurrentMapEntryTest {

    private static final String KEY1 = "key1";
    private static final String KEY2 = "key2";
    private static final String VALUE1 = "value1";
    private static final String VALUE2 = "value2";

    @Test
    void shouldCreateSingleInvertedMapEntry() {
        ConcurrentMapEntry<String, String> actual = new ConcurrentMapEntry<>();
        actual.setEntries(KEY1, VALUE1);

        String actualKey = actual.getKey();
        assertThat(actualKey).isEqualTo(KEY1);
        String actualValue = actual.getValue();
        assertThat(actualValue).isEqualTo(VALUE1);
    }

    @Test
    void shouldCreateMultipleEntries() {
        ConcurrentMapEntry<String, String> entry2 = new ConcurrentMapEntry<>();
        entry2.setEntries(KEY2, VALUE2);

        ConcurrentMapEntry<String, String> entry1 = new ConcurrentMapEntry<>();
        entry1.setEntries(KEY1, VALUE1);
        entry1.setNextEntry(entry2);

        ConcurrentMapEntry<String, String> actual = entry1.getNextEntry();

        String actualKey = actual.getKey();
        assertThat(actualKey).isEqualTo(KEY2);
        String actualValue = actual.getValue();
        assertThat(actualValue).isEqualTo(VALUE2);
    }

    @Test
    void shouldSetNextEntry() {
        ConcurrentMapEntry<String, String> entry2 = new ConcurrentMapEntry<>();
        entry2.setEntries(KEY2, VALUE2);

        ConcurrentMapEntry<String, String> entry1 = new ConcurrentMapEntry<>();
        entry1.setEntries(KEY1, VALUE1);
        entry1.setNextEntry(entry2);

        ConcurrentMapEntry<String, String> actual = entry1.getNextEntry();

        String actualKey = actual.getKey();
        assertThat(actualKey).isEqualTo(KEY2);
        String actualValue = actual.getValue();
        assertThat(actualValue).isEqualTo(VALUE2);
    }

    @Test
    void shouldGetAllEntries() {
        ConcurrentMapEntry<String, String> entry2 = new ConcurrentMapEntry<>();
        entry2.setEntries(KEY2, VALUE2);

        ConcurrentMapEntry<String, String> entry1 = new ConcurrentMapEntry<>();
        entry1.setEntries(KEY1, VALUE1);
        entry1.setNextEntry(entry2);

        List<ConcurrentMapEntry<String, String>> actual = entry1.getAllEntries();

        assertThat(actual).contains(entry1);
        assertThat(actual).contains(entry2);
    }

    @Test
    void shouldBeEqual() {
        ConcurrentMapEntry<String, String> entry1 = new ConcurrentMapEntry<>();
        entry1.setEntries(KEY1, VALUE1);

        ConcurrentMapEntry<String, String> entry2 = new ConcurrentMapEntry<>();
        entry2.setEntries(KEY1, VALUE1);

        boolean actual = entry1.equals(entry2);

        assertThat(actual).isTrue();
    }

    @Test
    void shouldBeNotEqual() {
        ConcurrentMapEntry<String, String> entry1 = new ConcurrentMapEntry<>();
        entry1.setEntries(KEY1, VALUE1);

        ConcurrentMapEntry<String, String> entry2 = new ConcurrentMapEntry<>();
        entry2.setEntries(KEY1, VALUE2);

        boolean actual = entry1.equals(entry2);

        assertThat(actual).isFalse();
    }
}