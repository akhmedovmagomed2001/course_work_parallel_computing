package com.ua.iasa.core.index;

import com.ua.iasa.core.context.FileIndexTaskContext;
import com.ua.iasa.core.set.ConcurrentHashSet;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinInvertedIndex implements InvertedIndex {

    private final CommonInvertedIndex commonInvertedIndex;
    private final ForkJoinPool forkJoinPool;

    public ForkJoinInvertedIndex(int threadsCount) {
        this.commonInvertedIndex = new CommonInvertedIndex();
        this.forkJoinPool = new ForkJoinPool(threadsCount);
    }

    @Override
    public void indexFile(File file) {
        indexFiles(List.of(file));
    }

    @Override
    public void indexFiles(List<File> filesToIndex) {
        FileIndexTaskContext context = new FileIndexTaskContext(commonInvertedIndex, filesToIndex);
        FileIndexTask fileIndexTask = new FileIndexTask(context);
        forkJoinPool.execute(fileIndexTask);

        fileIndexTask.join();
    }

    @Override
    public ConcurrentHashSet getFilesByWord(String word) {
        return commonInvertedIndex.getFilesByWord(word);
    }

    @Override
    public Set<String> getAllWords() {
        return commonInvertedIndex.getAllWords();
    }

    @Override
    public void clean() {
        commonInvertedIndex.clean();
    }

    @Override
    public void logStatistics() {
        commonInvertedIndex.logStatistics();
    }

}
