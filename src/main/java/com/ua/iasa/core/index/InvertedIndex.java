package com.ua.iasa.core.index;

import com.ua.iasa.core.set.ConcurrentHashSet;
import com.ua.iasa.core.util.InvertedIndexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public interface InvertedIndex {

    Logger LOG = LoggerFactory.getLogger(InvertedIndex.class);

    default void indexDir(String dirPath) {
        if (Objects.isNull(dirPath) || dirPath.isEmpty()) {
            throw new IllegalArgumentException("Dir path can not be empty");
        }

        try {
            List<File> filesToIndex = InvertedIndexUtils.getFiles(dirPath);

            String indexMessage = String.format("Indexing %s files in '%s' directory...", filesToIndex.size(), dirPath);
            LOG.info(indexMessage);

            indexFiles(filesToIndex);
        } catch (IOException ex) {
            String message = "Failed to index directory";
            LOG.error(message, ex);
        }
    }

    default void indexFiles(List<File> files) {
        files.forEach(this::indexFile);
    }

    void indexFile(File file);

    ConcurrentHashSet getFilesByWord(String word);

    Set<String> getAllWords();

    void clean();

    void logStatistics();

}
