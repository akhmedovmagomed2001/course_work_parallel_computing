package com.ua.iasa.core.index;

import com.ua.iasa.core.context.FileIndexTaskContext;

import java.io.File;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class FileIndexTask extends RecursiveAction {

    private static final int SPLIT_THRESHOLD = 20;

    private final transient FileIndexTaskContext context;

    public FileIndexTask(FileIndexTaskContext context) {
        this.context = context;
    }

    @Override
    protected void compute() {
        List<File> filesToIndex = context.getFilesToIndex();

        if (filesToIndex.size() < SPLIT_THRESHOLD) {
            CommonInvertedIndex commonInvertedIndex = context.getInvertedIndex();
            filesToIndex.forEach(commonInvertedIndex::indexFile);
        } else {
            split();
        }
    }

    private void split() {
        List<File> filesToIndex = context.getFilesToIndex();
        int halfSize = filesToIndex.size() / 2;

        List<File> lhs = filesToIndex.subList(0, halfSize);
        FileIndexTaskContext contextLeft = new FileIndexTaskContext(context.getInvertedIndex(), lhs);
        FileIndexTask fileIndexTaskLeft = new FileIndexTask(contextLeft);
        fileIndexTaskLeft.fork();

        List<File> rhs = filesToIndex.subList(halfSize, filesToIndex.size());
        FileIndexTaskContext contextRight = new FileIndexTaskContext(context.getInvertedIndex(), rhs);
        FileIndexTask fileIndexTaskRight = new FileIndexTask(contextRight);
        fileIndexTaskRight.compute();

        fileIndexTaskLeft.join();
    }
}
