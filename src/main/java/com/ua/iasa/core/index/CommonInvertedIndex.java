package com.ua.iasa.core.index;

import com.ua.iasa.core.map.ConcurrentMap;
import com.ua.iasa.core.map.MapApi;
import com.ua.iasa.core.set.ConcurrentHashSet;
import com.ua.iasa.core.util.InvertedIndexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class CommonInvertedIndex implements InvertedIndex{

    private static final Logger LOG = LoggerFactory.getLogger(CommonInvertedIndex.class);

    private MapApi<String, ConcurrentHashSet> index = new ConcurrentMap<>();
    private static final AtomicInteger INDEXED_FILES_COUNT = new AtomicInteger(0);

    @Override
    public void indexFile(File file) {
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String word = scanner.next();
                word = InvertedIndexUtils.filterCharactersInWord(word);
                word = word.toLowerCase();

                if (word.length() > 2) {
                    addWordToIndex(word, file.toString());
                }
            }

            INDEXED_FILES_COUNT.incrementAndGet();
        } catch (IOException ex) {
            String message = String.format("Failed to index file '%s'", file.getAbsolutePath());
            LOG.error(message, ex);
        }
    }

    private void addWordToIndex(String word, String filename) {
        if (!index.containsKey(word)) {
            index.put(word, new ConcurrentHashSet());
        }
        index.get(word).add(filename);
    }

    @Override
    public ConcurrentHashSet getFilesByWord(String word) {
        return index.get(word);
    }

    @Override
    public Set<String> getAllWords() {
        return index.keys();
    }

    @Override
    public void clean() {
        LOG.info("Cleaning inverted index...");

        index = new ConcurrentMap<>();
        INDEXED_FILES_COUNT.set(0);
    }

    @Override
    public void logStatistics() {
        String collisionsMessage = String.format("Collisions in the inverted index: %s", index.getCollisionCount());
        LOG.info(collisionsMessage);

        String sizeMessage = String.format("Inverted index allocation size: %s", index.size());
        LOG.info(sizeMessage);

        String indexedFilesMessage = String.format("Indexed files count: %s", INDEXED_FILES_COUNT.get());
        LOG.info(indexedFilesMessage);
    }

}
