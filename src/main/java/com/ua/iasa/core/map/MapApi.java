package com.ua.iasa.core.map;

import java.util.List;
import java.util.Set;

public interface MapApi<K, V> {

    void put(K key, V value);

    V get(K key);

    Set<K> keys();

    List<ConcurrentMapEntry<K, V>> entries();

    boolean containsKey(K key);

    int getCollisionCount();

    int size();

}
