package com.ua.iasa.core.map;

import com.ua.iasa.core.context.ConcurrentMapContext;
import com.ua.iasa.core.util.ConcurrentMapUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class ConcurrentMap<V> implements MapApi<String, V> {

    private double loadFactor = 0.75;
    private final AtomicInteger capacity = new AtomicInteger(10);
    private final AtomicInteger elementsCount = new AtomicInteger(0);
    private final AtomicInteger collisionCount = new AtomicInteger(0);

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    private ConcurrentMapEntry<String, V>[] entries;

    public ConcurrentMap(int capacity, double loadFactor) {
        this(capacity);
        this.loadFactor = loadFactor;
    }

    public ConcurrentMap(int capacity) {
        this.capacity.set(capacity);
        entries = ConcurrentMapUtils.createEmptyEntries(capacity);
    }

    public ConcurrentMap() {
        entries = ConcurrentMapUtils.createEmptyEntries(capacity.get());
    }

    @Override
    public void put(String key, V value) {

        if (needResize()) {
            try {
                writeLock.lock();
                if (needResize()) {
                    resize();
                }
            } finally {
                writeLock.unlock();
            }
        }

        try {
            readLock.lock();
            ConcurrentMapUtils.put(key, value, getContext());
        } finally {
            readLock.unlock();
        }
    }

    private boolean needResize() {
        return BigDecimal.valueOf(elementsCount.get())
                .divide(BigDecimal.valueOf(capacity.get()), 2, RoundingMode.HALF_UP)
                .compareTo(BigDecimal.valueOf(loadFactor)) > 0;
    }

    private void resize() {
        capacity.getAndUpdate(localCapacity -> localCapacity * 2);

        ConcurrentMapEntry<String, V>[] newEntries = ConcurrentMapUtils.createEmptyEntries(capacity.get());
        ConcurrentMapContext<V> newContext = new ConcurrentMapContext<>(newEntries, new AtomicInteger(0), new AtomicInteger(0));
        entries().forEach(concurrentMapEntry -> ConcurrentMapUtils.put(concurrentMapEntry.getKey(), concurrentMapEntry.getValue(), newContext));

        elementsCount.set(newContext.getElementsCount().get());
        collisionCount.set(newContext.getCollisionCount().get());
        this.entries = newContext.getConcurrentMapEntries();
    }

    @Override
    public V get(String key) {
        V result = null;
        try {
            readLock.lock();

            int index = ConcurrentMapUtils.generateIndex(key, this.capacity.get());
            ConcurrentMapEntry<String, V> invertedEntry = entries[index];

            while (
                    Objects.nonNull(invertedEntry) &&
                            Objects.nonNull(invertedEntry.getKey()) &&
                            !invertedEntry.getKey().equals(key)
            ) {
                invertedEntry = invertedEntry.getNextEntry();
            }

            if (Objects.nonNull(invertedEntry)) {
                result = invertedEntry.getValue();
            }
        } finally {
            readLock.unlock();
        }

        return result;
    }

    @Override
    public Set<String> keys() {
        ConcurrentMapEntry<String, V>[] currentEntities = getEntries();

        return Arrays.stream(currentEntities)
                .map(ConcurrentMapEntry::getAllEntries)
                .flatMap(Collection::stream)
                .map(ConcurrentMapEntry::getKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    @Override
    public List<ConcurrentMapEntry<String, V>> entries() {
        ConcurrentMapEntry<String, V>[] currentEntities = getEntries();
        return Arrays.stream(currentEntities)
                .map(ConcurrentMapEntry::getAllEntries)
                .flatMap(Collection::stream)
                .filter(entity -> Objects.nonNull(entity.getKey()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean containsKey(String key) {
        ConcurrentMapEntry<String, V>[] currentEntities = getEntries();
        return ConcurrentMapUtils.containsKey(key, currentEntities);
    }

    @Override
    public int getCollisionCount() {
        return collisionCount.get();
    }

    @Override
    public int size() {
        return capacity.get();
    }

    private ConcurrentMapEntry<String, V>[] getEntries() {
        try {
            readLock.lock();
            return this.entries;
        } finally {
            readLock.unlock();
        }
    }

    private ConcurrentMapContext<V> getContext() {
        try {
            readLock.lock();
            return new ConcurrentMapContext<>(this.entries, this.collisionCount, this.elementsCount);
        } finally {
            readLock.unlock();
        }
    }

    public double getLoadFactor() {
        return loadFactor;
    }

    public int getElementsCount() {
        return elementsCount.get();
    }

    @Override
    public String toString() {
        ConcurrentMapEntry<String, V>[] currentEntities = getEntries();
        return Arrays.stream(currentEntities)
                .filter(Objects::nonNull)
                .map(ConcurrentMapEntry::toString)
                .collect(Collectors.joining(", "));
    }
}
