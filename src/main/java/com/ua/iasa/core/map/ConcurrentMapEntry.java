package com.ua.iasa.core.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class ConcurrentMapEntry<K, V> {

    private K key;
    private V value;
    private ConcurrentMapEntry<K, V> nextEntry;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    public K getKey() {
        try {
            readLock.lock();
            return this.key;
        } finally {
            readLock.unlock();
        }
    }

    public V getValue() {
        try {
            readLock.lock();
            return this.value;
        } finally {
            readLock.unlock();
        }
    }

    public boolean init(K key, V value) {
        try {
            writeLock.lock();

            if (Objects.isNull(this.key)) {
                this.key = key;
                this.value = value;
                return true;
            } else {
                return false;
            }

        } finally {
            writeLock.unlock();
        }
    }


    /**
     * Not concurrent!
     */
    public void setEntries(K key, V value) {
        try {
            writeLock.lock();
            this.key = key;
            this.value = value;
        } finally {
            writeLock.unlock();
        }
    }

    public boolean setNextEntry(ConcurrentMapEntry<K, V> nextEntry) {
        try {
            writeLock.lock();

            if (Objects.isNull(this.nextEntry)) {
                this.nextEntry = nextEntry;
                return true;
            } else {
                return false;
            }
        } finally {
            writeLock.unlock();
        }
    }

    public ConcurrentMapEntry<K, V> getNextEntry() {
        try {
            readLock.lock();
            return this.nextEntry;
        } finally {
            readLock.unlock();
        }
    }

    public List<ConcurrentMapEntry<K, V>> getAllEntries() {
        List<ConcurrentMapEntry<K, V>> entries = new ArrayList<>();

        try {
            readLock.lock();

            ConcurrentMapEntry<K, V> currentEntry = this;
            do {
                entries.add(currentEntry);
                currentEntry = currentEntry.getNextEntry();
            }
            while (Objects.nonNull(currentEntry));
        } finally {
            readLock.unlock();
        }

        return entries;
    }

    @Override
    public String toString() {
        List<ConcurrentMapEntry<K, V>> entries = getAllEntries();

        String entryFormat = "[%s : %s]";
        return entries.stream()
                .map(entry -> String.format(entryFormat, entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(", "));
    }

    @Override
    public boolean equals(Object o) {
        try {
            readLock.lock();

            if (this == o) {
                return true;
            } else if (o == null || getClass() != o.getClass()) {
                return false;
            } else {
                ConcurrentMapEntry<?, ?> that = (ConcurrentMapEntry<?, ?>) o;
                return Objects.equals(key, that.key) && Objects.equals(value, that.value);
            }

        } finally {
            readLock.unlock();
        }
    }

    @Override
    public int hashCode() {
        try {
            readLock.lock();
            return Objects.hash(this.key, this.value);
        } finally {
            readLock.unlock();
        }
    }
}
