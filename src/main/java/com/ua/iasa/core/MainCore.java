package com.ua.iasa.core;

import com.ua.iasa.core.index.ForkJoinInvertedIndex;
import com.ua.iasa.core.index.InvertedIndex;
import com.ua.iasa.core.set.ConcurrentHashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainCore {

    private static final Logger LOG = LoggerFactory.getLogger(MainCore.class);

    private static final int INVERTED_INDEX_THREADS_COUNT = 4;
    private static final String DIR_TO_INDEX = "/tmp";

    public static void main(String[] args) {
        InvertedIndex invertedIndex = new ForkJoinInvertedIndex(INVERTED_INDEX_THREADS_COUNT);
        invertedIndex.indexDir(DIR_TO_INDEX);

        String wordToFind = "rabbit";
        ConcurrentHashSet filesByWord = invertedIndex.getFilesByWord(wordToFind);

        String messagePattern = "Files that contains word '%s': %s";
        String message = String.format(messagePattern, wordToFind, filesByWord);
        LOG.info(message);

        invertedIndex.logStatistics();
    }

}
