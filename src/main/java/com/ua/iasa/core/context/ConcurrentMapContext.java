package com.ua.iasa.core.context;

import com.ua.iasa.core.map.ConcurrentMapEntry;

import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentMapContext<V> {

    private final ConcurrentMapEntry<String, V>[] concurrentMapEntries;
    private final AtomicInteger collisionCount;
    private final AtomicInteger elementsCount;

    public ConcurrentMapContext(ConcurrentMapEntry<String, V>[] concurrentMapEntries, AtomicInteger collisionCount, AtomicInteger elementsCount) {
        this.concurrentMapEntries = concurrentMapEntries;
        this.collisionCount = collisionCount;
        this.elementsCount = elementsCount;
    }

    public ConcurrentMapEntry<String, V>[] getConcurrentMapEntries() {
        return concurrentMapEntries;
    }

    public AtomicInteger getCollisionCount() {
        return collisionCount;
    }

    public AtomicInteger getElementsCount() {
        return elementsCount;
    }
}
