package com.ua.iasa.core.context;

import com.ua.iasa.core.index.CommonInvertedIndex;

import java.io.File;
import java.util.List;

public class FileIndexTaskContext {

    private final CommonInvertedIndex commonInvertedIndex;
    private final List<File> filesToIndex;

    public FileIndexTaskContext(CommonInvertedIndex commonInvertedIndex, List<File> filesToIndex) {
        this.commonInvertedIndex = commonInvertedIndex;
        this.filesToIndex = filesToIndex;
    }

    public CommonInvertedIndex getInvertedIndex() {
        return commonInvertedIndex;
    }

    public List<File> getFilesToIndex() {
        return filesToIndex;
    }

}
