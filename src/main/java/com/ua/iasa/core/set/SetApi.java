package com.ua.iasa.core.set;

import java.util.Set;

public interface SetApi<E> {

    void add(E entity);

    boolean exist(E entity);

    int getElementsCount();

    Set<E> keys();

}
