package com.ua.iasa.core.set;

import com.ua.iasa.core.map.ConcurrentMap;

import java.util.Set;

public class ConcurrentHashSet implements SetApi<String> {

    private final ConcurrentMap<Object> map = new ConcurrentMap<>();

    @Override
    public void add(String entity) {
        if (!this.exist(entity)) {
            map.put(entity, null);
        }
    }

    @Override
    public boolean exist(String entity) {
        return map.containsKey(entity);
    }

    public int size() {
        return map.size();
    }

    @Override
    public int getElementsCount() {
        return map.getElementsCount();
    }

    @Override
    public Set<String> keys() {
        return map.keys();
    }

    @Override
    public String toString() {
        return map.keys().toString();
    }
}
