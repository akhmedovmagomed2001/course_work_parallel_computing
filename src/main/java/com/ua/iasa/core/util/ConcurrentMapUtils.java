package com.ua.iasa.core.util;

import com.ua.iasa.core.context.ConcurrentMapContext;
import com.ua.iasa.core.map.ConcurrentMapEntry;

import java.util.Objects;
import java.util.stream.Collectors;

public final class ConcurrentMapUtils {

    private ConcurrentMapUtils() {
    }

    public static <V> void put(String key, V value, ConcurrentMapContext<V> concurrentMapContext) {
        ConcurrentMapEntry<String, V>[] concurrentMapEntries = concurrentMapContext.getConcurrentMapEntries();
        int index = generateIndex(key, concurrentMapEntries.length);

        boolean isKeyAlreadyExist = containsKey(key, concurrentMapEntries);
        if (isKeyAlreadyExist) {
            ConcurrentMapEntry<String, V> concurrentMapEntry = concurrentMapEntries[index];

            while (!concurrentMapEntry.getKey().equals(key)) {
                concurrentMapEntry = concurrentMapEntry.getNextEntry();
            }

            concurrentMapEntry.setEntries(key, value);
        } else {
            if (Objects.nonNull(concurrentMapEntries[index].getKey())) {
                ConcurrentMapEntry<String, V> currentEntry = concurrentMapEntries[index];

                ConcurrentMapEntry<String, V> concurrentMapEntry = new ConcurrentMapEntry<>();
                concurrentMapEntry.setEntries(key, value);

                while (!currentEntry.setNextEntry(concurrentMapEntry)) {
                    currentEntry = currentEntry.getNextEntry();
                }

                concurrentMapContext.getCollisionCount().incrementAndGet();
                concurrentMapContext.getElementsCount().incrementAndGet();
            } else {
                boolean isInitialized = concurrentMapEntries[index].init(key, value);
                if (!isInitialized) {
                    put(key, value, concurrentMapContext);
                } else {
                    concurrentMapContext.getElementsCount().incrementAndGet();
                }
            }
        }
    }

    public static int generateIndex(String key, int capacity) {
        int index = hashString(key) % capacity;
        if (index < 0) {
            index *= -1;
        }
        return index;
    }

    private static int hashString(String s) {
        int h = 0;
        if (s.length() > 0) {
            char[] val = s.toCharArray();

            for (int i = 0; i < s.length(); i++) {
                h = 31 * h + val[i];
            }
        }
        return h;
    }

    public static <V> boolean containsKey(String key, ConcurrentMapEntry<String, V>[] concurrentMapEntries) {
        int index = generateIndex(key, concurrentMapEntries.length);
        if (Objects.isNull(concurrentMapEntries[index].getKey())) {
            return false;
        }

        return concurrentMapEntries[index].getAllEntries()
                .stream()
                .map(ConcurrentMapEntry::getKey)
                .collect(Collectors.toList()).contains(key);
    }

    public static <V> ConcurrentMapEntry<String, V>[] createEmptyEntries(int capacity) {
        @SuppressWarnings("unchecked") ConcurrentMapEntry<String, V>[] newEntries = new ConcurrentMapEntry[capacity];

        for (int i = 0; i < capacity; i++) {
            newEntries[i] = new ConcurrentMapEntry<>();
        }
        return newEntries;
    }

}
