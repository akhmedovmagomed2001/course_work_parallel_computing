package com.ua.iasa.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class InvertedIndexUtils {

    private static final String CHARS_TO_FILTER = ".,'\";:”“‘’—!?() _*#[]";

    private InvertedIndexUtils() {
    }

    public static String filterCharactersInWord(String word) {
        for (char characterToFilter : CHARS_TO_FILTER.toCharArray()) {
            word = word.replace(String.valueOf(characterToFilter), "");
        }
        return word;
    }

    public static List<File> getFiles(String dirPath) throws IOException {
        File file = new File(dirPath);

        if (!file.exists()) {
            throw new FileNotFoundException("No such file or directory");
        }

        if (!file.isDirectory()) {
            throw new IOException("File is not a directory");
        }

        return Arrays.stream(Objects.requireNonNull(file.listFiles()))
                .filter(File::isFile).collect(Collectors.toList());
    }
}
