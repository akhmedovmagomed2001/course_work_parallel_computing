package com.ua.iasa.server.rpc.controller;

import com.google.gson.Gson;
import com.ua.iasa.common.model.response.FindWordResponse;
import com.ua.iasa.common.model.response.SingleMessage;
import com.ua.iasa.core.index.ForkJoinInvertedIndex;
import com.ua.iasa.core.index.InvertedIndex;
import com.ua.iasa.core.set.ConcurrentHashSet;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import com.ua.iasa.server.rpc.context.ResponseContext;

import java.io.File;
import java.util.Optional;
import java.util.Set;

public class InvertedIndexController {

    private static InvertedIndexController instance;

    private final InvertedIndex invertedIndex;

    private InvertedIndexController(int threadsCount) {
        this.invertedIndex = new ForkJoinInvertedIndex(threadsCount);
    }

    public ResponseContext indexDir(String pathToDir) {
        invertedIndex.indexDir(pathToDir);
        String message = String.format("Path '%s' is successfully indexed", pathToDir);
        String body = new SingleMessage(message).toJson();
        return new ResponseContext(body, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext indexFile(String pathToFile) {
        File fileToIndex = new File(pathToFile);
        invertedIndex.indexFile(fileToIndex);
        String message = String.format("File '%s' is successfully indexed", fileToIndex);
        String body = new SingleMessage(message).toJson();
        return new ResponseContext(body, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext findWord(String word) {
        Set<String> files = Optional.ofNullable(invertedIndex.getFilesByWord(word))
                .orElse(new ConcurrentHashSet())
                .keys();

        String message = "Search is done";
        FindWordResponse findWordResponse = new FindWordResponse(message, word, files);
        String body = new Gson().toJson(findWordResponse);
        return new ResponseContext(body, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext clean() {
        invertedIndex.clean();
        String message = "Clean is done";
        String body = new SingleMessage(message).toJson();
        return new ResponseContext(body, ResponseConstants.SUCCESS_CODE);
    }

    public static void create(int threadsCount) {
        instance = new InvertedIndexController(threadsCount);
    }

    public static InvertedIndexController getInstance() {
        return instance;
    }
}
