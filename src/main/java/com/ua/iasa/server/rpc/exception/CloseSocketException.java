package com.ua.iasa.server.rpc.exception;

public class CloseSocketException extends RuntimeException {
    public CloseSocketException(Throwable cause) {
        super(cause);
    }
}
