package com.ua.iasa.server.rpc.service;

import com.ua.iasa.common.context.ServerConfigContext;
import com.ua.iasa.common.util.FileUtils;
import com.ua.iasa.crypto.util.AsymmetricEncryptionUtils;

import java.security.Key;

public class KeyManagementService {

    private static KeyManagementService instance;

    private final Key publicKey;
    private final Key privateKey;

    private KeyManagementService(ServerConfigContext serverConfigContext) {
        String publicKeyFilePath = serverConfigContext.getPublicKeyFilePath();
        byte[] publicKeyBytes = FileUtils.readFromFile(publicKeyFilePath);
        this.publicKey = AsymmetricEncryptionUtils.getExistingKey(publicKeyBytes, true);

        String privateKeyFilePath = serverConfigContext.getPrivateKeyFilePath();
        byte[] privateKeyBytes = FileUtils.readFromFile(privateKeyFilePath);
        this.privateKey = AsymmetricEncryptionUtils.getExistingKey(privateKeyBytes, false);
    }

    public Key getPublicKey() {
        return publicKey;
    }

    public Key getPrivateKey() {
        return privateKey;
    }

    public static KeyManagementService getInstance() {
        return instance;
    }

    public static void create(ServerConfigContext serverConfigContext) {
        instance = new KeyManagementService(serverConfigContext);
    }
}
