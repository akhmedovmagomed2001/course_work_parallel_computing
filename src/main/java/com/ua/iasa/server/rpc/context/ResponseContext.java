package com.ua.iasa.server.rpc.context;

import com.ua.iasa.server.rpc.constants.ResponseConstants;

public class ResponseContext {

    private final String body;
    private final int status;
    private final String reason;

    public ResponseContext(String body, int status) {
        this.body = body;
        this.status = status;
        this.reason = ResponseConstants.convertStatusToReason(status);
    }

    public String getBody() {
        return body;
    }

    public int getStatus() {
        return status;
    }

    public String getReason() {
        return reason;
    }
}
