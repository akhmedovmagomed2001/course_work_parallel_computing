package com.ua.iasa.server.rpc.constants;

public final class ServerConstants {

    private ServerConstants() {
    }

    public static final String SERVER_NAME = "InvertedServer";
    public static final String DEFAULT_CONTENT_TYPE = "application/json";
    public static final String DEFAULT_HTTP_VERSION = "HTTP/1.1";

}
