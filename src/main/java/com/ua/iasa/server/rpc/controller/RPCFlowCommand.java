package com.ua.iasa.server.rpc.controller;

import com.google.gson.Gson;
import com.ua.iasa.common.model.request.FindWordBody;
import com.ua.iasa.common.model.request.InvertedIndexCommand;
import com.ua.iasa.common.model.response.InvertedIndexResult;
import com.ua.iasa.common.model.response.SingleMessage;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import com.ua.iasa.server.rpc.context.ResponseContext;
import com.ua.iasa.server.rpc.model.request.IndexDirBody;
import com.ua.iasa.server.rpc.model.request.IndexFileBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.ua.iasa.common.constant.OperationConstants.CLEAN_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.FIND_WORD_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_DIR_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_FILE_OPERATION;

public class RPCFlowCommand {

    private static final Logger LOG = LoggerFactory.getLogger(RPCFlowCommand.class);

    private static RPCFlowCommand instance;

    private final Map<String, Function<String, InvertedIndexResult>> flowMap = new HashMap<>();
    private final InvertedIndexController invertedIndexController = InvertedIndexController.getInstance();

    public RPCFlowCommand() {
        flowMap.put(INDEX_DIR_OPERATION, this::indexDirFlow);
        flowMap.put(INDEX_FILE_OPERATION, this::indexFileFlow);
        flowMap.put(FIND_WORD_OPERATION, this::findFlow);
        flowMap.put(CLEAN_OPERATION, this::cleanFlow);
    }

    public InvertedIndexResult processCommand(InvertedIndexCommand invertedIndexCommand) {
        String operation = invertedIndexCommand.getOperation();
        String body = invertedIndexCommand.getBody();

        if (!flowMap.containsKey(operation)) {
            String message = String.format("Can not handle this operation '%s'", operation);
            String responseBody = new SingleMessage(message).toJson();
            return new InvertedIndexResult(ResponseConstants.NOT_FOUNT_REASON, responseBody);
        }

        try {
            return flowMap.get(operation).apply(body);
        } catch (Exception ex) {
            LOG.error("Internal server error", ex);
            String message = String.format("Reason: %s", ex.getMessage());
            String responseBody = new SingleMessage(message).toJson();
            return new InvertedIndexResult(ResponseConstants.INTERNAL_SERVER_ERROR_REASON, responseBody);
        }
    }

    private InvertedIndexResult indexDirFlow(String body) {
        IndexDirBody indexDirBody = new Gson().fromJson(body, IndexDirBody.class);
        ResponseContext responseContext = invertedIndexController.indexDir(indexDirBody.getPathToDir());
        return new InvertedIndexResult(responseContext);
    }

    private InvertedIndexResult indexFileFlow(String body) {
        IndexFileBody indexFileBody = new Gson().fromJson(body, IndexFileBody.class);
        ResponseContext responseContext = invertedIndexController.indexFile(indexFileBody.getPathToFile());
        return new InvertedIndexResult(responseContext);
    }

    private InvertedIndexResult findFlow(String body) {
        FindWordBody findWordBody = new Gson().fromJson(body, FindWordBody.class);
        ResponseContext responseContext = invertedIndexController.findWord(findWordBody.getWord());
        return new InvertedIndexResult(responseContext);
    }

    private InvertedIndexResult cleanFlow(String body) {
        ResponseContext responseContext = invertedIndexController.clean();
        return new InvertedIndexResult(responseContext);
    }

    public static RPCFlowCommand getInstance() {
        if (instance == null) {
            instance = new RPCFlowCommand();
        }

        return instance;
    }
}
