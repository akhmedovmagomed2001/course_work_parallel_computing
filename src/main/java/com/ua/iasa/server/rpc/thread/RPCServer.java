package com.ua.iasa.server.rpc.thread;

import com.ua.iasa.common.context.ServerConfigContext;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.server.rpc.exception.CloseSocketException;
import com.ua.iasa.common.exception.OpenSocketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class RPCServer implements ShutdownRunnable {

    private static final Logger LOG = LoggerFactory.getLogger(RPCServer.class);
    private final AtomicBoolean isEnabled = new AtomicBoolean(true);

    private final ExecutorService executorService;
    private final ServerSocket serverSocket;

    public RPCServer(ServerConfigContext serverConfigContext) {
        int handlersCountValue = serverConfigContext.getHandlersCount();
        this.executorService = Executors.newFixedThreadPool(handlersCountValue);

        try {
            int cryptoServerPort = serverConfigContext.getServerPort();
            String serverHost = serverConfigContext.getServerHost();
            InetSocketAddress address = new InetSocketAddress(serverHost, cryptoServerPort);
            this.serverSocket = new ServerSocket();
            serverSocket.bind(address);
        } catch (IOException ex) {
            throw new OpenSocketException(ex);
        }
    }

    @Override
    public void run() {
        LOG.info("Starting the RPC server...");

        try (this.serverSocket) {
            while (isEnabled.get() && serverSocket.isBound() && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                RPCHandler rpcHandler = new RPCHandler(socket);
                executorService.submit(rpcHandler);
            }
        } catch (SocketException ex) {
            LOG.error("Socket is interrupted. Reason: {}", ex.getMessage());
        } catch (IOException ex) {
            LOG.error("I/O exception during the execution", ex);
        }

        LOG.info("Shutdown the RPC server...");
    }

    @Override
    public void shutdown() {
        isEnabled.set(false);
        try {
            serverSocket.close();
        } catch (IOException ex) {
            throw new CloseSocketException(ex);
        }

        shutDownExecutorService();
    }

    private void shutDownExecutorService() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            executorService.shutdownNow();
        }
    }
}
