package com.ua.iasa.server.rpc;

import com.ua.iasa.common.context.ServerConfigContext;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.common.thread.Timer;
import com.ua.iasa.common.util.ConfigUtils;
import com.ua.iasa.server.rpc.controller.InvertedIndexController;
import com.ua.iasa.server.rpc.service.KeyManagementService;
import com.ua.iasa.server.rpc.thread.RPCServer;

import java.util.List;
import java.util.Properties;

import static com.ua.iasa.common.constant.ConfigConstant.CONFIG_FILE_ENVIRONMENT_VARIABLE;

public class MainServer {

    public static void main(String[] args) {
        Properties configs = ConfigUtils.parseConfigsByEnvironmentVariable(CONFIG_FILE_ENVIRONMENT_VARIABLE);
        ServerConfigContext serverConfigContext = new ServerConfigContext(configs);
        init(serverConfigContext);

        RPCServer rpcServer = new RPCServer(serverConfigContext);

        List<ShutdownRunnable> instances = List.of(rpcServer);
        Timer timer = new Timer(instances, serverConfigContext.getWorkTime());

        Thread timerThread = new Thread(timer);
        timerThread.start();

        try {
            timerThread.join();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private static void init(ServerConfigContext serverConfigContext) {
        InvertedIndexController.create(serverConfigContext.getInvertedIndexThreadCount());
        KeyManagementService.create(serverConfigContext);
    }

}
