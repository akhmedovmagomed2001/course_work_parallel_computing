package com.ua.iasa.server.rpc.model.request;

public class IndexDirBody {
    private final String pathToDir;

    public IndexDirBody(String pathToDir) {
        this.pathToDir = pathToDir;
    }

    public String getPathToDir() {
        return pathToDir;
    }
}
