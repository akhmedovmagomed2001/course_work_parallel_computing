package com.ua.iasa.server.rpc.constants;

public final class PathConstants {

    private PathConstants() {
    }

    public static final String INDEX_DIR_PATH = "/index/dir";
    public static final String INDEX_FILE_PATH = "/index/file";
    public static final String FIND_PATH = "/find";
    public static final String CLEAN_PATH = "/clean";

}
