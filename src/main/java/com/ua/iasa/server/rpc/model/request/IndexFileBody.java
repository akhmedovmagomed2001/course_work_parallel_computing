package com.ua.iasa.server.rpc.model.request;

public class IndexFileBody {

    private final String pathToFile ;

    public IndexFileBody(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public String getPathToFile() {
        return pathToFile;
    }
}
