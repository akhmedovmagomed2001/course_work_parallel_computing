package com.ua.iasa.server.rpc.thread;

import com.google.gson.Gson;
import com.ua.iasa.common.model.request.InvertedIndexCommand;
import com.ua.iasa.common.model.response.InvertedIndexResult;
import com.ua.iasa.common.util.NetworkUtils;
import com.ua.iasa.crypto.context.EncryptionData;
import com.ua.iasa.crypto.context.EncryptionDataRaw;
import com.ua.iasa.crypto.exception.CryptoException;
import com.ua.iasa.crypto.util.AsymmetricEncryptionUtils;
import com.ua.iasa.crypto.util.SymmetricEncryptionUtils;
import com.ua.iasa.server.rpc.controller.RPCFlowCommand;
import com.ua.iasa.server.rpc.service.KeyManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.Key;

public class RPCHandler implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(RPCHandler.class);

    private final Socket socket;
    private final RPCFlowCommand rpcFlowCommand = RPCFlowCommand.getInstance();
    private final KeyManagementService keyManagementService = KeyManagementService.getInstance();

    public RPCHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        LOG.info("Request occurred");

        try (InputStream inputStream = socket.getInputStream(); OutputStream outputStream = socket.getOutputStream()) {
            EncryptionData encryptionData = keyExchangeServer(inputStream, outputStream);

            byte[] inputDataBytesEncrypted = NetworkUtils.readInputData(inputStream);
            byte[] inputDataBytes = SymmetricEncryptionUtils.decrypt(inputDataBytesEncrypted, encryptionData);

            byte[] outputDataBytes = process(inputDataBytes);
            byte[] outputDataBytesEncrypted = SymmetricEncryptionUtils.encrypt(outputDataBytes, encryptionData);

            outputStream.write(outputDataBytesEncrypted);
        } catch (Exception ex) {
            LOG.error("Exception during the processing.", ex);
        }

        LOG.info("Request processed");
    }

    private byte[] process(byte[] inputDataBytes) {
        String inputData = new String(inputDataBytes);
        InvertedIndexCommand invertedIndexCommand = new Gson().fromJson(inputData, InvertedIndexCommand.class);

        LOG.info("Inverted index command: {}", invertedIndexCommand);

        InvertedIndexResult invertedIndexResult = rpcFlowCommand.processCommand(invertedIndexCommand);
        String invertedIndexResultJson = new Gson().toJson(invertedIndexResult);

        return invertedIndexResultJson.getBytes();
    }

    private EncryptionData keyExchangeServer(InputStream inputStream, OutputStream outputStream) throws IOException {
        Key publicKey = keyManagementService.getPublicKey();
        byte[] publicKeyBytes = publicKey.getEncoded();

        outputStream.write(publicKeyBytes);
        byte[] sessionKeyEncrypted = NetworkUtils.readInputData(inputStream);

        if (sessionKeyEncrypted.length == 0) {
            throw new CryptoException("Client input is empty");
        }

        Key privateKey = keyManagementService.getPrivateKey();
        String sessionKeyJson = AsymmetricEncryptionUtils.decrypt(sessionKeyEncrypted, privateKey);
        EncryptionDataRaw encryptionDataRaw = new Gson().fromJson(sessionKeyJson, EncryptionDataRaw.class);

        if (encryptionDataRaw.isEmpty()) {
            throw new CryptoException("Encryption data is empty");
        }

        EncryptionData encryptionData = new EncryptionData(encryptionDataRaw);
        outputStream.write(1);

        return encryptionData;
    }
}
