package com.ua.iasa.server.rpc.constants;

import com.ua.iasa.common.constant.HeadersConstants;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class ResponseConstants {

    private ResponseConstants() {
    }

    public static final int SUCCESS_CODE = 200;
    public static final String SUCCESS_REASON = "OK";

    public static final int BAD_REQUEST_CODE = 400;
    public static final String BAD_REQUEST_REASON = "Bad Request";

    public static final int NOT_FOUNT_CODE = 404;
    public static final String NOT_FOUNT_REASON = "Not Found";

    public static final int INTERNAL_SERVER_ERROR_CODE = 500;
    public static final String INTERNAL_SERVER_ERROR_REASON = "Internal server error";

    private static final Map<Integer, String> STATUS_TO_REASON_MAP = new HashMap<>();

    static {
        STATUS_TO_REASON_MAP.put(SUCCESS_CODE, SUCCESS_REASON);
        STATUS_TO_REASON_MAP.put(BAD_REQUEST_CODE, BAD_REQUEST_REASON);
        STATUS_TO_REASON_MAP.put(NOT_FOUNT_CODE, NOT_FOUNT_REASON);
        STATUS_TO_REASON_MAP.put(INTERNAL_SERVER_ERROR_CODE, INTERNAL_SERVER_ERROR_REASON);
    }

    public static String convertStatusToReason(int status) {
        return STATUS_TO_REASON_MAP.get(status);
    }

    public static Map<String, String> getDefaultResponseHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put(HeadersConstants.DATE_HEADER, new Date().toString());
        headers.put(HeadersConstants.SERVER_HEADER, ServerConstants.SERVER_NAME);
        headers.put(HeadersConstants.CONNECTION_HEADER, "Closed");

        return headers;
    }
}
