package com.ua.iasa.server.rpc.thread;

import java.io.IOException;
import java.net.Socket;

public interface Handler extends Runnable {

    void handle(Socket socket) throws IOException;
}
