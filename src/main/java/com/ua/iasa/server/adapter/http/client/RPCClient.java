package com.ua.iasa.server.adapter.http.client;

import com.google.gson.Gson;
import com.ua.iasa.common.context.HttpAdapterConfigContext;
import com.ua.iasa.common.exception.OpenSocketException;
import com.ua.iasa.common.model.request.InvertedIndexCommand;
import com.ua.iasa.common.model.response.InvertedIndexResult;
import com.ua.iasa.common.util.NetworkUtils;
import com.ua.iasa.crypto.context.EncryptionData;
import com.ua.iasa.crypto.util.SymmetricEncryptionUtils;
import com.ua.iasa.server.adapter.http.exception.RCPClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;

public class RPCClient {

    private static final Logger LOG = LoggerFactory.getLogger(RPCClient.class);

    private static RPCClient instance;

    private final int invertedIndexPort;
    private final String invertedIndexHost;

    public RPCClient(HttpAdapterConfigContext httpAdapterConfigContext) {
        this.invertedIndexPort = httpAdapterConfigContext.getInvertedServerPort();
        this.invertedIndexHost = httpAdapterConfigContext.getInvertedServerHost();
    }

    public InvertedIndexResult execute(InvertedIndexCommand invertedIndexCommand) {
        try (Socket socket = new Socket(invertedIndexHost, invertedIndexPort)) {
            return process(socket, invertedIndexCommand);
        } catch (ConnectException ex) {
            String address = String.format("%s:%d", invertedIndexHost, invertedIndexPort);
            String message = String.format("Connection to inverted server is refused. Address: %s", address);
            throw new OpenSocketException(message);
        } catch (IOException ex) {
            throw new OpenSocketException(ex);
        }
    }

    private InvertedIndexResult process(Socket socket, InvertedIndexCommand invertedIndexCommand) {
        try (OutputStream outputStream = socket.getOutputStream(); InputStream inputStream = socket.getInputStream()) {
            EncryptionData encryptionData = NetworkUtils.keyExchangeClient(inputStream, outputStream);

            String invertedIndexCommandJson = new Gson().toJson(invertedIndexCommand);
            byte[] outputDataBytes = invertedIndexCommandJson.getBytes();
            byte[] outputDataBytesEncrypted = SymmetricEncryptionUtils.encrypt(outputDataBytes, encryptionData);
            outputStream.write(outputDataBytesEncrypted);

            byte[] inputDataBytesEncrypted = NetworkUtils.readInputData(inputStream);
            byte[] inputDataBytes = SymmetricEncryptionUtils.decrypt(inputDataBytesEncrypted, encryptionData);
            String inputData = new String(inputDataBytes);

            return new Gson().fromJson(inputData, InvertedIndexResult.class);
        } catch (Exception ex) {
            throw new RCPClientException(ex);
        }
    }

    public static RPCClient getInstance() {
        return instance;
    }

    public static void create(HttpAdapterConfigContext httpAdapterConfigContext) {
        instance = new RPCClient(httpAdapterConfigContext);
    }
}
