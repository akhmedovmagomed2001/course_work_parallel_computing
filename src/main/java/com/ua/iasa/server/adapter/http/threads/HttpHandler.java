package com.ua.iasa.server.adapter.http.threads;

import com.ua.iasa.common.constant.HeadersConstants;
import com.ua.iasa.common.exception.InvalidHttpDataFormatException;
import com.ua.iasa.common.model.http.Request;
import com.ua.iasa.common.model.http.RequestTitle;
import com.ua.iasa.common.model.http.Response;
import com.ua.iasa.common.model.http.ResponseTitle;
import com.ua.iasa.common.util.HttpDataUtils;
import com.ua.iasa.common.util.NetworkUtils;
import com.ua.iasa.server.adapter.http.controller.FlowCommand;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import com.ua.iasa.server.rpc.constants.ServerConstants;
import com.ua.iasa.server.rpc.context.ResponseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Objects;

public class HttpHandler implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(HttpHandler.class);

    private final Socket socket;
    private final FlowCommand flowCommand = FlowCommand.getInstance();

    public HttpHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (socket) {
            LOG.info("Request occurred");
            handle(socket);
            LOG.info("Request processed");
        } catch (IOException ex) {
            LOG.error("I/O exception during the execution", ex);
        }
    }

    public void handle(Socket socket) throws IOException {
        try (InputStream inputStream = socket.getInputStream(); OutputStream outputStream = socket.getOutputStream()) {
            byte[] inputDataBytes = NetworkUtils.readInputData(inputStream);
            byte[] outputDataBytes = process(inputDataBytes);
            outputStream.write(outputDataBytes);
        } catch (Exception ex) {
            LOG.error("Exception during the processing.", ex);
        }
    }

    public byte[] process(byte[] inputDataBytes) {
        String inputData = new String(inputDataBytes);
        LOG.debug("Request:\n{}", inputData);

        Request request = parseRequest(inputData);
        ResponseContext responseContext = flowCommand.processRequest(request);
        Response response = createResponse(responseContext);
        String outputData = response.convertToRawString();

        LOG.debug("Response:\n{}", outputData);
        return outputData.getBytes();
    }

    private Response createResponse(ResponseContext responseContext) {
        ResponseTitle title = new ResponseTitle(ServerConstants.DEFAULT_HTTP_VERSION, responseContext.getStatus(), responseContext.getReason());

        Map<String, String> headers = ResponseConstants.getDefaultResponseHeaders();

        String body = responseContext.getBody();
        if (!body.isEmpty()) {
            headers.put(HeadersConstants.CONTENT_TYPE_HEADER, ServerConstants.DEFAULT_CONTENT_TYPE);
            headers.put(HeadersConstants.CONTENT_LENGTH_HEADER, String.valueOf(body.length()));
        }

        return new Response(title, headers, body);
    }

    private Request parseRequest(String request) {
        if (Objects.isNull(request) || request.isEmpty()) {
            throw new InvalidHttpDataFormatException("Empty request");
        }

        RequestTitle requestTitle = HttpDataUtils.parseRequestTitle(request);
        Map<String, String> headers = HttpDataUtils.parseHeaders(request);
        String body = HttpDataUtils.parseBody(request);

        return new Request(requestTitle, headers, body);
    }

}
