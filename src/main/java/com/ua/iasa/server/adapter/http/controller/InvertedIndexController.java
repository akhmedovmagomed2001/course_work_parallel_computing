package com.ua.iasa.server.adapter.http.controller;

import com.google.gson.Gson;
import com.ua.iasa.common.model.request.FindWordBody;
import com.ua.iasa.common.model.request.InvertedIndexCommand;
import com.ua.iasa.common.model.response.InvertedIndexResult;
import com.ua.iasa.common.model.response.SingleMessage;
import com.ua.iasa.server.adapter.http.client.RPCClient;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import com.ua.iasa.server.rpc.context.ResponseContext;
import com.ua.iasa.server.rpc.model.request.IndexDirBody;
import com.ua.iasa.server.rpc.model.request.IndexFileBody;

import java.io.File;
import java.util.Objects;

import static com.ua.iasa.common.constant.OperationConstants.CLEAN_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.FIND_WORD_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_DIR_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_FILE_OPERATION;

public class InvertedIndexController {

    private static InvertedIndexController instance;

    private final RPCClient rpcClient = RPCClient.getInstance();

    public ResponseContext indexDir(String pathToDir) {
        if (Objects.isNull(pathToDir) || pathToDir.isEmpty()) {
            String body = new SingleMessage("Path to dir is empty").toJson();
            return new ResponseContext(body, ResponseConstants.BAD_REQUEST_CODE);
        }

        IndexDirBody indexDirBody = new IndexDirBody(pathToDir);
        String body = new Gson().toJson(indexDirBody);

        InvertedIndexCommand invertedIndexCommand = new InvertedIndexCommand(INDEX_DIR_OPERATION, body);
        InvertedIndexResult invertedIndexResult = rpcClient.execute(invertedIndexCommand);
        String responseBodyJson = invertedIndexResult.getResponseBody();

        return new ResponseContext(responseBodyJson, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext indexFile(String pathToFile) {
        if (Objects.isNull(pathToFile) || pathToFile.isEmpty() || pathToFile.isBlank()) {
            String body = new SingleMessage("Path to dir is empty").toJson();
            return new ResponseContext(body, ResponseConstants.BAD_REQUEST_CODE);
        }

        File fileToIndex = new File(pathToFile);

        if (!fileToIndex.exists()) {
            String body = new SingleMessage("File does not exits").toJson();
            return new ResponseContext(body, ResponseConstants.BAD_REQUEST_CODE);
        }

        if (fileToIndex.isDirectory()) {
            String body = new SingleMessage("File is a directory").toJson();
            return new ResponseContext(body, ResponseConstants.BAD_REQUEST_CODE);
        }

        IndexFileBody indexFileBody = new IndexFileBody(pathToFile);
        String body = new Gson().toJson(indexFileBody);
        InvertedIndexCommand invertedIndexCommand = new InvertedIndexCommand(INDEX_FILE_OPERATION, body);
        InvertedIndexResult invertedIndexResult = rpcClient.execute(invertedIndexCommand);
        String responseBodyJson = invertedIndexResult.getResponseBody();

        return new ResponseContext(responseBodyJson, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext findWord(String word) {
        FindWordBody findWordBody = new FindWordBody(word);
        String body = new Gson().toJson(findWordBody);
        InvertedIndexCommand invertedIndexCommand = new InvertedIndexCommand(FIND_WORD_OPERATION, body);
        InvertedIndexResult invertedIndexResult = rpcClient.execute(invertedIndexCommand);
        String responseBodyJson = invertedIndexResult.getResponseBody();

        return new ResponseContext(responseBodyJson, ResponseConstants.SUCCESS_CODE);
    }

    public ResponseContext clean() {
        InvertedIndexCommand invertedIndexCommand = new InvertedIndexCommand(CLEAN_OPERATION, "");
        InvertedIndexResult invertedIndexResult = rpcClient.execute(invertedIndexCommand);
        String responseBodyJson = invertedIndexResult.getResponseBody();
        return new ResponseContext(responseBodyJson, ResponseConstants.SUCCESS_CODE);
    }

    public static void create() {
        instance = new InvertedIndexController();
    }

    public static InvertedIndexController getInstance() {
        return instance;
    }
}
