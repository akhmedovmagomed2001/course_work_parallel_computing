package com.ua.iasa.server.adapter.http;

import com.ua.iasa.common.context.HttpAdapterConfigContext;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.common.thread.Timer;
import com.ua.iasa.common.util.ConfigUtils;
import com.ua.iasa.server.adapter.http.client.RPCClient;
import com.ua.iasa.server.adapter.http.controller.FlowCommand;
import com.ua.iasa.server.adapter.http.controller.InvertedIndexController;
import com.ua.iasa.server.adapter.http.threads.HttpAdapter;

import java.util.List;
import java.util.Properties;

import static com.ua.iasa.common.constant.ConfigConstant.CONFIG_FILE_ENVIRONMENT_VARIABLE;

public class MainHttpAdapter {

    public static void main(String[] args) {
        Properties configs = ConfigUtils.parseConfigsByEnvironmentVariable(CONFIG_FILE_ENVIRONMENT_VARIABLE);
        HttpAdapterConfigContext httpAdapterConfigContext = new HttpAdapterConfigContext(configs);
        init(httpAdapterConfigContext);

        ShutdownRunnable instance = new HttpAdapter(httpAdapterConfigContext);
        Timer timer = new Timer(List.of(instance), httpAdapterConfigContext.getWorkTime());

        Thread timerThread = new Thread(timer);

        timerThread.start();

        try {
            timerThread.join();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private static void init(HttpAdapterConfigContext httpAdapterConfigContext) {
        RPCClient.create(httpAdapterConfigContext);
        InvertedIndexController.create();
        FlowCommand.create();
    }
}
