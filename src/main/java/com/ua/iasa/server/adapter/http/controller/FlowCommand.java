package com.ua.iasa.server.adapter.http.controller;

import com.google.gson.Gson;
import com.ua.iasa.common.model.http.Request;
import com.ua.iasa.common.model.request.FindWordBody;
import com.ua.iasa.common.model.response.SingleMessage;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import com.ua.iasa.server.rpc.context.ResponseContext;
import com.ua.iasa.server.rpc.model.request.IndexDirBody;
import com.ua.iasa.server.rpc.model.request.IndexFileBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.ua.iasa.server.rpc.constants.PathConstants.CLEAN_PATH;
import static com.ua.iasa.server.rpc.constants.PathConstants.FIND_PATH;
import static com.ua.iasa.server.rpc.constants.PathConstants.INDEX_DIR_PATH;
import static com.ua.iasa.server.rpc.constants.PathConstants.INDEX_FILE_PATH;

public class FlowCommand {

    private static final Logger LOG = LoggerFactory.getLogger(FlowCommand.class);

    private static FlowCommand instance;

    private final Map<String, Function<String, ResponseContext>> flowMap = new HashMap<>();
    private final InvertedIndexController invertedIndexController;

    private FlowCommand() {
        flowMap.put(INDEX_DIR_PATH, this::indexDirFlow);
        flowMap.put(INDEX_FILE_PATH, this::indexFileFlow);
        flowMap.put(FIND_PATH, this::findFlow);
        flowMap.put(CLEAN_PATH, this::cleanFlow);

        this.invertedIndexController = InvertedIndexController.getInstance();
    }

    public ResponseContext processRequest(Request request) {
        String path = request.getTitle().getPath();
        String body = request.getBody();

        LOG.info("Processing request with '{}' path", path);

        if (!flowMap.containsKey(path)) {
            String message = String.format("Can not handle this path '%s'", path);
            String responseBody = new SingleMessage(message).toJson();
            return new ResponseContext(responseBody, ResponseConstants.NOT_FOUNT_CODE);
        }

        try {
            return flowMap.get(path).apply(body);
        } catch (Exception ex) {
            LOG.error("Internal server error", ex);
            String message = String.format("Reason: %s", ex.getMessage());
            String responseBody = new SingleMessage(message).toJson();
            return new ResponseContext(responseBody, ResponseConstants.INTERNAL_SERVER_ERROR_CODE);
        }
    }

    private ResponseContext indexDirFlow(String body) {
        IndexDirBody indexDirBody = new Gson().fromJson(body, IndexDirBody.class);
        return invertedIndexController.indexDir(indexDirBody.getPathToDir());
    }

    private ResponseContext indexFileFlow(String body) {
        IndexFileBody indexFileBody = new Gson().fromJson(body, IndexFileBody.class);
        return invertedIndexController.indexFile(indexFileBody.getPathToFile());
    }

    private ResponseContext findFlow(String body) {
        FindWordBody findWordBody = new Gson().fromJson(body, FindWordBody.class);
        return invertedIndexController.findWord(findWordBody.getWord());
    }

    private ResponseContext cleanFlow(String body) {
        return invertedIndexController.clean();
    }

    public static void create() {
        instance = new FlowCommand();
    }

    public static FlowCommand getInstance() {
        return instance;
    }
}