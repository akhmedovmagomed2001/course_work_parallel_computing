package com.ua.iasa.server.adapter.http.threads;

import com.ua.iasa.common.context.HttpAdapterConfigContext;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.server.rpc.exception.CloseSocketException;
import com.ua.iasa.common.exception.OpenSocketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class HttpAdapter implements ShutdownRunnable {

    private static final Logger LOG = LoggerFactory.getLogger(HttpAdapter.class);
    private final AtomicBoolean isEnabled = new AtomicBoolean(true);

    private final ExecutorService executorService;
    private final ServerSocket serverSocket;

    public HttpAdapter(HttpAdapterConfigContext httpAdapterConfigContext) {
        int handlersCountValue = httpAdapterConfigContext.getHandlersCount();
        this.executorService = Executors.newFixedThreadPool(handlersCountValue);

        try {
            int serverPort = httpAdapterConfigContext.getServerPort();
            String serverHost = httpAdapterConfigContext.getServerHost();
            InetSocketAddress address = new InetSocketAddress(serverHost, serverPort);
            this.serverSocket = new ServerSocket();
            serverSocket.bind(address);
        } catch (IOException ex) {
            throw new OpenSocketException(ex);
        }
    }

    @Override
    public void run() {
        LOG.info("Starting the default server...");

        try (this.serverSocket) {
            while (isEnabled.get() && serverSocket.isBound() && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                HttpHandler httpHandler = new HttpHandler(socket);
                executorService.submit(httpHandler);
            }
        } catch (SocketException ex) {
            LOG.error("Socket is interrupted. Reason: {}", ex.getMessage());
        } catch (IOException ex) {
            LOG.error("I/O exception during the execution", ex);
        }

        LOG.info("Shutdown the default server...");
    }


    @Override
    public void shutdown() {
        isEnabled.set(false);
        try {
            serverSocket.close();
        } catch (IOException ex) {
            throw new CloseSocketException(ex);
        }
    }
}
