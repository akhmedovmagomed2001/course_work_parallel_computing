package com.ua.iasa.server.adapter.http.exception;

public class RCPClientException extends RuntimeException {

    public RCPClientException(Throwable cause) {
        super(cause);
    }

}
