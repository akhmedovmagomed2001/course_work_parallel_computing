package com.ua.iasa.crypto.context;

import com.ua.iasa.crypto.util.SymmetricEncryptionUtils;

import javax.crypto.spec.IvParameterSpec;
import java.security.Key;
import java.util.Base64;

public class EncryptionData {
    private final Key key;
    private final IvParameterSpec iv;

    public EncryptionData(Key key, IvParameterSpec iv) {
        this.key = key;
        this.iv = iv;
    }

    public EncryptionData(EncryptionDataRaw encryptionDataRaw) {
        String keyBase64 = encryptionDataRaw.getKey();
        byte[] keyValue = Base64.getDecoder().decode(keyBase64);
        this.key = SymmetricEncryptionUtils.getExistingKey(keyValue);

        String ivBase64 = encryptionDataRaw.getIv();
        byte[] ivValue = Base64.getDecoder().decode(ivBase64);
        this.iv = new IvParameterSpec(ivValue);
    }

    public Key getKey() {
        return key;
    }

    public IvParameterSpec getIv() {
        return iv;
    }

    @Override
    public String toString() {
        String keyStr = toBytesString(key.getEncoded());
        String ivStr = toBytesString(iv.getIV());

        return "EncryptionData:\n" +
                "Key: " + keyStr + "\n" +
                "IV: " + ivStr;
    }

    private static String toBytesString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02X ", b));
        }
        return result.toString();
    }

}
