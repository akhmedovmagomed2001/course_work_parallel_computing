package com.ua.iasa.crypto.context;

import com.google.gson.Gson;

import java.util.Base64;
import java.util.Objects;

public class EncryptionDataRaw {

    private final String iv;
    private final String key;

    public EncryptionDataRaw(EncryptionData encryptionData) {
        byte[] ivValue = encryptionData.getIv().getIV();
        this.iv = Base64.getEncoder().encodeToString(ivValue);

        byte[] keyValue = encryptionData.getKey().getEncoded();
        this.key = Base64.getEncoder().encodeToString(keyValue);
    }

    public String getIv() {
        return iv;
    }

    public String getKey() {
        return key;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public boolean isEmpty() {
        return iv.isEmpty() || key.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EncryptionDataRaw that = (EncryptionDataRaw) o;
        return Objects.equals(iv, that.iv) && Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iv, key);
    }
}
