package com.ua.iasa.crypto.util;

import com.ua.iasa.crypto.exception.CryptoException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static com.ua.iasa.crypto.constants.EncryptionConstants.ASYMMETRIC_CYPHER_TRANSFORMATION;

public final class AsymmetricEncryptionUtils {

    private AsymmetricEncryptionUtils() {
    }

    public static byte[] encrypt(String input, Key publicKey) {
        try {
            Cipher encryptCipher = Cipher.getInstance(ASYMMETRIC_CYPHER_TRANSFORMATION);
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] inputBytes = input.getBytes();
            return encryptCipher.doFinal(inputBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException |
                 BadPaddingException ex) {
            throw new CryptoException(ex);
        }
    }

    public static String decrypt(byte[] input, Key privateKey) {
        try {
            Cipher decryptCipher = Cipher.getInstance(ASYMMETRIC_CYPHER_TRANSFORMATION);
            decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decryptedBytes = decryptCipher.doFinal(input);

            return new String(decryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException |
                 BadPaddingException ex) {
            throw new CryptoException(ex);
        }
    }

    public static Key getExistingKey(byte[] keyBytes, boolean isPublic) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ASYMMETRIC_CYPHER_TRANSFORMATION);
            if (isPublic) {
                EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
                return keyFactory.generatePublic(keySpec);
            } else {
                EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
                return keyFactory.generatePrivate(keySpec);
            }
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            throw new CryptoException(ex);
        }
    }
}
