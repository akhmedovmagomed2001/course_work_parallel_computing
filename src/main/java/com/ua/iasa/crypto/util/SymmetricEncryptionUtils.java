package com.ua.iasa.crypto.util;

import com.ua.iasa.crypto.constants.EncryptionConstants;
import com.ua.iasa.crypto.context.EncryptionData;
import com.ua.iasa.crypto.exception.CryptoException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static com.ua.iasa.crypto.constants.EncryptionConstants.SYMMETRIC_KEY_BIT_COUNT;

public final class SymmetricEncryptionUtils {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private SymmetricEncryptionUtils() {
    }

    public static byte[] encrypt(byte[] inputDataBytes, EncryptionData encryptionData) {
        try {
            Cipher cipher = Cipher.getInstance(EncryptionConstants.SYMMETRIC_CYPHER_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, encryptionData.getKey(), encryptionData.getIv());

            return cipher.doFinal(inputDataBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                 InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException(ex);
        }
    }

    public static byte[] decrypt(byte[] input, EncryptionData encryptionData) {
        try {
            Cipher cipher = Cipher.getInstance(EncryptionConstants.SYMMETRIC_CYPHER_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, encryptionData.getKey(), encryptionData.getIv());

            return cipher.doFinal(input);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                 InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException(ex);
        }
    }

    public static EncryptionData createEncryptionData() {
        Key secretKey;
        try {
            secretKey = getRandomKey();
        } catch (NoSuchAlgorithmException ex) {
            throw new CryptoException(ex);
        }

        byte[] initialVector = generateInitialVector();

        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        return new EncryptionData(secretKey, ivParameterSpec);
    }

    private static Key getRandomKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(EncryptionConstants.SYMMETRIC_ENCRYPTION_ALGORITHM);
        keyGenerator.init(SYMMETRIC_KEY_BIT_COUNT, SECURE_RANDOM);
        return keyGenerator.generateKey();
    }

    private static byte[] generateInitialVector() {
        byte[] result = new byte[16];
        SECURE_RANDOM.nextBytes(result);
        return result;
    }

    public static Key getExistingKey(byte[] key) {
        return new SecretKeySpec(key, EncryptionConstants.SYMMETRIC_ENCRYPTION_ALGORITHM);
    }

}
