package com.ua.iasa.crypto.constants;

public final class EncryptionConstants {

    private EncryptionConstants() {
    }

    public static final String SYMMETRIC_CYPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    public static final String SYMMETRIC_ENCRYPTION_ALGORITHM = "AES";
    public static final int SYMMETRIC_KEY_BIT_COUNT = 256;

    public static final String ASYMMETRIC_CYPHER_TRANSFORMATION = "RSA";

}
