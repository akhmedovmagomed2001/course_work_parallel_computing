package com.ua.iasa.common.constant;

public final class ConfigConstant {

    private ConfigConstant() {
    }

    /**
     * Environment variables
     */
    public static final String CONFIG_FILE_ENVIRONMENT_VARIABLE = "INVERTED_INDEX_CFG";

    /**
     * RPC Server configs
     */
    public static final String SERVER_INVERTED_INDEX_THREADS_COUNT_CONFIG = "SERVER_INVERTED_INDEX_THREADS_COUNT";
    public static final String SERVER_PUBLIC_KEY_FILE_PATH_CONFIG = "SERVER_PUBLIC_KEY_FILE_PATH";
    public static final String SERVER_PRIVATE_KEY_FILE_PATH_CONFIG = "SERVER_PRIVATE_KEY_FILE_PATH";

    /**
     * Client configs
     */
    public static final String CLIENT_THREADS_COUNT_CONFIG = "CLIENT_THREADS_COUNT";
    public static final String WORDS_FILE_PATH_CONFIG = "WORDS_FILE_PATH";
    public static final String MAX_DELAY_CONFIG = "MAX_DELAY";

    /**
     * General configs
     */
    public static final String SERVER_PORT_CONFIG = "SERVER_PORT";
    public static final String SERVER_HOST_CONFIG = "SERVER_HOST";
    public static final String INVERTED_SERVER_PORT_CONFIG = "INVERTED_SERVER_PORT";
    public static final String INVERTED_SERVER_HOST_CONFIG = "INVERTED_SERVER_HOST";
    public static final String SERVER_HANDLERS_COUNT_CONFIG = "SERVER_HANDLERS_COUNT";
    public static final String WORK_TIME_CONFIG = "WORK_TIME";

}
