package com.ua.iasa.common.constant;

public final class OperationConstants {

    private OperationConstants() {
    }

    public static final String FIND_WORD_OPERATION = "findWord";
    public static final String INDEX_FILE_OPERATION = "indexFile";
    public static final String INDEX_DIR_OPERATION = "indexDir";
    public static final String CLEAN_OPERATION = "clean";

}
