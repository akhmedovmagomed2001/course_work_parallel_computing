package com.ua.iasa.common.constant;

public final class HeadersConstants {

    private HeadersConstants() {
    }

    public static final String SERVER_HEADER = "Server";
    public static final String DATE_HEADER = "Date";
    public static final String CONTENT_TYPE_HEADER = "Content-Type";
    public static final String CONTENT_LENGTH_HEADER = "Content-Length";
    public static final String CONNECTION_HEADER = "Connection";
    public static final String USER_AGENT_HEADER = "User-Agent";
    public static final String HOST_HEADER = "Host";

}
