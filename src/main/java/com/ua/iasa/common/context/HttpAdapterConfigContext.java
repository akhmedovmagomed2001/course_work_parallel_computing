package com.ua.iasa.common.context;

import com.ua.iasa.common.constant.ConfigConstant;
import com.ua.iasa.common.util.ConfigUtils;

import java.util.Properties;

public class HttpAdapterConfigContext {

    private final int serverPort;
    private final String serverHost;
    private final int invertedServerPort;
    private final String invertedServerHost;
    private final int handlersCount;
    private final int workTime;

    public HttpAdapterConfigContext(Properties configs) {
        String serverPortValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_PORT_CONFIG);
        this.serverPort = Integer.parseInt(serverPortValue);

        this.serverHost = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_HOST_CONFIG);

        String invertedServerPortValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.INVERTED_SERVER_PORT_CONFIG);
        this.invertedServerPort = Integer.parseInt(invertedServerPortValue);

        this.invertedServerHost = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.INVERTED_SERVER_HOST_CONFIG);

        String handlersCountValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_HANDLERS_COUNT_CONFIG);
        this.handlersCount = Integer.parseInt(handlersCountValue);

        String workTimeValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.WORK_TIME_CONFIG);
        this.workTime = Integer.parseInt(workTimeValue);
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getServerHost() {
        return serverHost;
    }

    public int getInvertedServerPort() {
        return invertedServerPort;
    }

    public String getInvertedServerHost() {
        return invertedServerHost;
    }

    public int getHandlersCount() {
        return handlersCount;
    }

    public int getWorkTime() {
        return workTime;
    }
}
