package com.ua.iasa.common.context;

import com.ua.iasa.common.constant.ConfigConstant;
import com.ua.iasa.common.util.ConfigUtils;

import java.util.Properties;

public class ServerConfigContext {

    private final int serverPort;
    private final String serverHost;
    private final int workTime;
    private final int invertedIndexThreadCount;
    private final int handlersCount;
    private final String publicKeyFilePath;
    private final String privateKeyFilePath;

    public ServerConfigContext(Properties configs) {
        String serverPortValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_PORT_CONFIG);
        this.serverPort = Integer.parseInt(serverPortValue);

        this.serverHost = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_HOST_CONFIG);

        String workTimeValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.WORK_TIME_CONFIG);
        this.workTime = Integer.parseInt(workTimeValue);

        String invertedIndexThreadCountValue =
                ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_INVERTED_INDEX_THREADS_COUNT_CONFIG);
        this.invertedIndexThreadCount = Integer.parseInt(invertedIndexThreadCountValue);

        String handlersCountValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_HANDLERS_COUNT_CONFIG);
        this.handlersCount = Integer.parseInt(handlersCountValue);

        this.publicKeyFilePath =
                ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_PUBLIC_KEY_FILE_PATH_CONFIG);

        this.privateKeyFilePath =
                ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_PRIVATE_KEY_FILE_PATH_CONFIG);
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getServerHost() {
        return serverHost;
    }

    public int getWorkTime() {
        return workTime;
    }

    public int getInvertedIndexThreadCount() {
        return invertedIndexThreadCount;
    }

    public int getHandlersCount() {
        return handlersCount;
    }

    public String getPublicKeyFilePath() {
        return publicKeyFilePath;
    }

    public String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }
}

