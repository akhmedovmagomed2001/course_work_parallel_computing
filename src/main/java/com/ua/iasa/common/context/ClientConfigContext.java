package com.ua.iasa.common.context;

import com.ua.iasa.common.constant.ConfigConstant;
import com.ua.iasa.common.util.ConfigUtils;

import java.util.Properties;

public class ClientConfigContext {

    private final int invertedIndexPort;
    private final String invertedIndexHost;
    private final int workTime;
    private final int threadsCount;
    private final String wordsFilePath;
    private final int maxDelay;

    public ClientConfigContext(Properties configs) {
        String invertedIndexPortValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_PORT_CONFIG);
        this.invertedIndexPort = Integer.parseInt(invertedIndexPortValue);

        this.invertedIndexHost = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.SERVER_HOST_CONFIG);

        String workTimeValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.WORK_TIME_CONFIG);
        this.workTime = Integer.parseInt(workTimeValue);

        String threadsCountValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.CLIENT_THREADS_COUNT_CONFIG);
        this.threadsCount = Integer.parseInt(threadsCountValue);

        this.wordsFilePath = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.WORDS_FILE_PATH_CONFIG);

        String maxDelayValue = ConfigUtils.retrieveConfigValue(configs, ConfigConstant.MAX_DELAY_CONFIG);
        this.maxDelay = Integer.parseInt(maxDelayValue);
    }

    public int getInvertedIndexPort() {
        return invertedIndexPort;
    }

    public String getInvertedIndexHost() {
        return invertedIndexHost;
    }

    public int getWorkTime() {
        return workTime;
    }

    public int getThreadsCount() {
        return threadsCount;
    }

    public String getWordsFilePath() {
        return wordsFilePath;
    }

    public int getMaxDelay() {
        return maxDelay;
    }
}
