package com.ua.iasa.common.util;

import com.ua.iasa.common.exception.InvalidHttpDataFormatException;
import com.ua.iasa.common.model.http.RequestTitle;
import com.ua.iasa.common.model.http.ResponseTitle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public final class HttpDataUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HttpDataUtils.class);

    private HttpDataUtils() {
    }

    public static ResponseTitle parseResponseTitle(String data) {
        RequestTitle requestTitle = parseRequestTitle(data);

        int code = Integer.parseInt(requestTitle.getPath());
        String httpVersion = requestTitle.getMethod();
        String reason = requestTitle.getHttpVersion();

        return new ResponseTitle(httpVersion, code, reason);
    }

    public static RequestTitle parseRequestTitle(String data) {
        String title;
        try (Scanner scanner = new Scanner(data)) {
            title = scanner.nextLine();
        }

        if (Objects.isNull(title) || title.isEmpty()) {
            throw new InvalidHttpDataFormatException("Empty data title");
        }

        String[] titleProperties = title.split(" ");

        if (titleProperties.length != 3) {
            String messagePattern = "Invalid data title format. It contains more/less then 3 entries '%s'";
            String message = String.format(messagePattern, title);
            throw new InvalidHttpDataFormatException(message);
        }

        String method = titleProperties[0];
        String path = titleProperties[1];
        String httpVersion = titleProperties[2];

        return new RequestTitle(method, path, httpVersion);
    }

    public static Map<String, String> parseHeaders(String data) {
        List<String> headerList = new ArrayList<>();

        try (Scanner scanner = new Scanner(data)) {
            scanner.nextLine();
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.isEmpty()) {
                    break;
                }

                headerList.add(line);
            }
        }

        Map<String, String> headers = new HashMap<>();
        for (String header : headerList) {
            String[] split = header.split(": ");

            if (split.length != 2) {
                LOG.warn("Can not parse header '{}'", header);
                continue;
            }

            String headerKey = split[0].trim();
            String headerValue = split[1].trim();
            headers.put(headerKey, headerValue);
        }

        return headers;
    }

    public static String parseBody(String data) {
        StringBuilder bodyBuilder = new StringBuilder();

        try (Scanner scanner = new Scanner(data)) {
            while (scanner.hasNext()) {
                if (scanner.nextLine().isEmpty()) {
                    break;
                }
            }

            while (scanner.hasNext()) {
                String bodyLine = scanner.nextLine().trim();
                bodyBuilder.append(bodyLine);
            }
        }

        return bodyBuilder.toString();
    }

    public static String convertHeaders(Map<String, String> headers) {
        return headers.entrySet().stream()
                .map(entry -> String.format("%s: %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining("\n"));
    }
}
