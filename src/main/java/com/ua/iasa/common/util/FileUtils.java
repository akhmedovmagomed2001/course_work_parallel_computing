package com.ua.iasa.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class FileUtils {

    private FileUtils() {
    }

    public static byte[] readFromFile(String filename) {
        File resourceFile = new File(filename);
        byte[] result;

        try (FileInputStream fileInputStream = new FileInputStream(resourceFile)) {
            result = new byte[fileInputStream.available()];
            fileInputStream.read(result);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

}
