package com.ua.iasa.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

public final class ConfigUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigUtils.class);

    private ConfigUtils() {
    }

    public static Properties parseConfigsByEnvironmentVariable(String environmentVariable) {
        String configFilePath = getEnvironmentVariableValue(environmentVariable);
        try {
            return parseConfigs(configFilePath);
        } catch (IOException ex) {
            String message = String.format("Can not parse configs from path '%s'", configFilePath);
            throw new IllegalStateException(message, ex);
        }
    }

    public static String getEnvironmentVariableValue(String environmentVariableKey) {
        String value = System.getenv(environmentVariableKey);

        if (Objects.isNull(value) || value.isEmpty() || value.isBlank()) {
            String message = String.format("Environment variable '%s' value is not present", environmentVariableKey);
            throw new IllegalStateException(message);
        }

        return value;
    }

    public static Properties parseConfigs(String configFilePath) throws IOException {
        File file = new File(configFilePath);

        if (!file.exists()) {
            String message = String.format("Config file '%s' does not exists", configFilePath);
            LOG.error(message);
            throw new FileNotFoundException(message);
        }

        if (file.isDirectory()) {
            String message = String.format("Config file '%s' is a directory", configFilePath);
            LOG.error(message);
            throw new IOException(message);
        }

        Properties config;
        try (FileReader fileReader = new FileReader(file)) {
            config = new Properties();
            config.load(fileReader);
            LOG.info("Input configs: {}", config);
        } catch (IOException ex) {
            throw new IOException("Failed to load config", ex);
        }

        return config;
    }

    public static String retrieveConfigValue(Properties configs, String configKey) {
        String configValue = Optional.ofNullable(configs.getProperty(configKey))
                .orElseThrow(() -> new IllegalStateException(
                        String.format("Config value with key '%s' is not present", configKey)
                ))
                .trim();
        validateConfig(configKey, configValue);
        return configValue;
    }

    public static void validateConfig(String configKey, String configValue) {
        if (Objects.isNull(configValue) || configValue.isEmpty()) {
            String message = String.format("Config '%s' has invalid value '%s'", configKey, configValue);
            LOG.error(message);
            throw new InvalidParameterException(message);
        }
    }
}
