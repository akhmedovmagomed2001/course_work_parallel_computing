package com.ua.iasa.common.util;

import com.ua.iasa.crypto.context.EncryptionData;
import com.ua.iasa.crypto.context.EncryptionDataRaw;
import com.ua.iasa.crypto.util.AsymmetricEncryptionUtils;
import com.ua.iasa.crypto.util.SymmetricEncryptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

public final class NetworkUtils {

    private static final Logger LOG = LoggerFactory.getLogger(NetworkUtils.class);

    private NetworkUtils() {
    }

    public static byte[] readInputData(InputStream inputStream) throws IOException {
        List<Byte> byteList = new ArrayList<>();

        do {
            int read = inputStream.read();
            byteList.add((byte) read);
        } while (inputStream.available() != 0);

        return convertListToArray(byteList);
    }

    private static byte[] convertListToArray(List<Byte> byteList) {
        byte[] bytes = new byte[byteList.size()];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = byteList.get(i);
        }

        return bytes;
    }

    public static EncryptionData keyExchangeClient(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] serverPublicKeyRaw = NetworkUtils.readInputData(inputStream);
        Key serverPublicKey = AsymmetricEncryptionUtils.getExistingKey(serverPublicKeyRaw, true);

        EncryptionData encryptionData = SymmetricEncryptionUtils.createEncryptionData();
        String encryptionDataJson = new EncryptionDataRaw(encryptionData).toJson();

        byte[] encryptedSymmetricKey = AsymmetricEncryptionUtils.encrypt(encryptionDataJson, serverPublicKey);

        outputStream.write(encryptedSymmetricKey);

        byte[] bytes = NetworkUtils.readInputData(inputStream);
        if (bytes.length == 1 && bytes[0] == 1) {
            LOG.debug("Server key exchange is successful");
        }

        return encryptionData;
    }

}
