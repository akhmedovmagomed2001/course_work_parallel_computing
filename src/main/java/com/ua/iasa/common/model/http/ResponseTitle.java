package com.ua.iasa.common.model.http;

public class ResponseTitle {
    private final String httpVersion;
    private final int code;
    private final String reason;

    public ResponseTitle(String httpVersion, int code, String reason) {
        this.httpVersion = httpVersion;
        this.code = code;
        this.reason = reason;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public int getCode() {
        return code;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public String toString() {
        String pattern = "%s %d %s";
        return String.format(pattern, httpVersion, code, reason);
    }
}
