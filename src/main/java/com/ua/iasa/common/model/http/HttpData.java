package com.ua.iasa.common.model.http;

import com.ua.iasa.common.util.HttpDataUtils;

import java.util.Map;

public interface HttpData {

    String HTTP_DATA_PATTERN = "%s\n%s\n\n%s";
    String HTTP_DATA_EMPTY_BODY_PATTERN = "%s\n%s\n\n";

    String getTitleString();

    Map<String, String> getHeaders();

    String getBody();

    default String convertToRawString() {
        Map<String, String> headers = getHeaders();
        String convertedHeaders = HttpDataUtils.convertHeaders(headers);

        String titleString = getTitleString();
        String body = getBody();

        if (body.isEmpty()) {
            return String.format(HTTP_DATA_EMPTY_BODY_PATTERN, titleString, convertedHeaders);
        } else {
            return String.format(HTTP_DATA_PATTERN, titleString, convertedHeaders, body);
        }
    }
}
