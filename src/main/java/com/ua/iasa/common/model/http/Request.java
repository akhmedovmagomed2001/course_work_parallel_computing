package com.ua.iasa.common.model.http;

import java.util.Map;

public class Request implements HttpData {
    private final RequestTitle title;
    private final Map<String, String> headers;
    private final String body;

    public Request(RequestTitle title, Map<String, String> headers, String body) {
        this.title = title;
        this.headers = headers;
        this.body = body;
    }

    public RequestTitle getTitle() {
        return title;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public String getTitleString() {
        return getTitle().toString();
    }
}
