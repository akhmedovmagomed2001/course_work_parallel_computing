package com.ua.iasa.common.model.request;

public class InvertedIndexCommand {

    private String operation;
    private String body;

    public InvertedIndexCommand(String operation, String body) {
        this.operation = operation;
        this.body = body;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "InvertedIndexCommand{" +
                "operation='" + operation + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
