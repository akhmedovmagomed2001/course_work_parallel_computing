package com.ua.iasa.common.model.request;

public class FindWordBody {

    private final String word;

    public FindWordBody(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }
}
