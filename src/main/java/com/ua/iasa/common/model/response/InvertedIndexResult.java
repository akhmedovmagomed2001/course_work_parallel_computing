package com.ua.iasa.common.model.response;

import com.ua.iasa.server.rpc.context.ResponseContext;

public class InvertedIndexResult {

    private final String status;
    private final String responseBody;

    public InvertedIndexResult(String status, String responseBody) {
        this.status = status;
        this.responseBody = responseBody;
    }

    public InvertedIndexResult(ResponseContext responseContext) {
        this.status = responseContext.getReason();
        this.responseBody = responseContext.getBody();
    }

    public String getStatus() {
        return status;
    }

    public String getResponseBody() {
        return responseBody;
    }
}
