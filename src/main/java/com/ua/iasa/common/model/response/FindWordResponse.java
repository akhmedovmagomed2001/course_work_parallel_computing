package com.ua.iasa.common.model.response;

import java.util.Set;

public class FindWordResponse {
    private final String message;
    private final String word;
    private final Set<String> files;

    public FindWordResponse(String message, String word, Set<String> files) {
        this.message = message;
        this.word = word;
        this.files = files;
    }

    public String getMessage() {
        return message;
    }

    public String getWord() {
        return word;
    }

    public Set<String> getFiles() {
        return files;
    }
}
