package com.ua.iasa.common.model.http;

public class RequestTitle {
    private final String method;
    private final String path;
    private final String httpVersion;

    public RequestTitle(String method, String path, String httpVersion) {
        this.method = method;
        this.path = path;
        this.httpVersion = httpVersion;
    }

    public String getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    @Override
    public String toString() {
        String pattern = "%s %s %s";
        return String.format(pattern, method, path, httpVersion);
    }
}
