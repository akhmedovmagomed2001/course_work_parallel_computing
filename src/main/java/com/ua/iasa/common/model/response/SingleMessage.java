package com.ua.iasa.common.model.response;

import com.google.gson.Gson;

public class SingleMessage {

    private final String message;

    public SingleMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

}
