package com.ua.iasa.common.model.http;

import java.util.Map;

public class Response implements HttpData {
    private final ResponseTitle title;
    private final Map<String, String> headers;
    private final String body;

    public Response(ResponseTitle title, Map<String, String> headers, String body) {
        this.title = title;
        this.headers = headers;
        this.body = body;
    }

    public ResponseTitle getTitle() {
        return title;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public String getTitleString() {
        return getTitle().toString();
    }
}
