package com.ua.iasa.common.exception;

public class OpenSocketException extends RuntimeException {

    public OpenSocketException(String message) {
        super(message);
    }

    public OpenSocketException(Throwable cause) {
        super(cause);
    }
}
