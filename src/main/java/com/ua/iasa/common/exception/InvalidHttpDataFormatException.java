package com.ua.iasa.common.exception;

public class InvalidHttpDataFormatException extends RuntimeException {
    public InvalidHttpDataFormatException(String message) {
        super(message);
    }
}
