package com.ua.iasa.common.thread;

public interface ShutdownRunnable extends Runnable {
    void shutdown();
}
