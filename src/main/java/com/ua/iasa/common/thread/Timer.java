package com.ua.iasa.common.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Timer implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(Timer.class);

    private static final String START_MESSAGE = "Timer is started";
    private static final String END_MESSAGE = "Shutting down instances";

    private final List<ShutdownRunnable> instances;
    private final long idleTime;

    public Timer(List<ShutdownRunnable> instances, int idleMinutes) {
        this.instances = instances;
        this.idleTime = (long) idleMinutes * 1000 * 60;
    }

    @Override
    public void run() {
        LOG.info(START_MESSAGE);
        long idleTimeMinutes = idleTime / 60000;
        String shutdownMessage = String.format("Timer will shutdown instances after %s minutes", idleTimeMinutes);
        LOG.info(shutdownMessage);

        try {
            instances.stream().map(Thread::new).forEach(Thread::start);

            Thread.sleep(idleTime);
            LOG.info(END_MESSAGE);

            instances.forEach(ShutdownRunnable::shutdown);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();

            String threadName = Thread.currentThread().getName();
            String message = String.format("[%s] Thread is interrupted!", threadName);
            LOG.info(message);
        }
    }
}