package com.ua.iasa.client.thread;

import com.google.gson.Gson;
import com.ua.iasa.client.service.FindWordRequestService;
import com.ua.iasa.client.service.RCPResponseFlowCommand;
import com.ua.iasa.common.context.ClientConfigContext;
import com.ua.iasa.common.model.request.InvertedIndexCommand;
import com.ua.iasa.common.model.response.InvertedIndexResult;
import com.ua.iasa.common.model.response.SingleMessage;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.common.util.NetworkUtils;
import com.ua.iasa.crypto.context.EncryptionData;
import com.ua.iasa.crypto.util.SymmetricEncryptionUtils;
import com.ua.iasa.server.rpc.constants.ResponseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.ua.iasa.common.constant.OperationConstants.FIND_WORD_OPERATION;

public class RPCClient implements ShutdownRunnable {

    private static final Logger LOG = LoggerFactory.getLogger(RPCClient.class);
    private final AtomicBoolean isEnabled = new AtomicBoolean(true);
    private final SecureRandom secureRandom = new SecureRandom();

    private final int invertedIndexPort;
    private final String invertedIndexHost;
    private final RCPResponseFlowCommand rcpResponseFlowCommand = RCPResponseFlowCommand.getInstance();
    private final FindWordRequestService findWordRequestService = FindWordRequestService.getInstance();
    private final int maxDelay;

    public RPCClient(ClientConfigContext clientConfigContext) {
        this.invertedIndexPort = clientConfigContext.getInvertedIndexPort();
        this.invertedIndexHost = clientConfigContext.getInvertedIndexHost();
        this.maxDelay = clientConfigContext.getMaxDelay();
    }

    @Override
    public void run() {
        LOG.info("Starting the client...");

        while (isEnabled.get()) {
            try (Socket socket = new Socket(invertedIndexHost, invertedIndexPort)) {
                process(socket);
            } catch (ConnectException ex) {
                String address = String.format("%s:%d", invertedIndexHost, invertedIndexPort);
                LOG.error("Connection to inverted server is refused. Address: {}", address);
            }
            catch (IOException ex) {
                LOG.error("I/O exception during the execution", ex);
            }

            doRequestDelay();
        }

        LOG.info("Shutdown the client...");
    }

    private void process(Socket socket) {
        try (OutputStream outputStream = socket.getOutputStream(); InputStream inputStream = socket.getInputStream()) {
            EncryptionData encryptionData = NetworkUtils.keyExchangeClient(inputStream, outputStream);

            InvertedIndexCommand findWordRequest = findWordRequestService.createFindWordRequest();
            String invertedIndexCommand = new Gson().toJson(findWordRequest);

            byte[] outputDataBytes = invertedIndexCommand.getBytes();
            byte[] outputDataBytesEncrypted = SymmetricEncryptionUtils.encrypt(outputDataBytes, encryptionData);
            outputStream.write(outputDataBytesEncrypted);

            byte[] inputDataBytesEncrypted = NetworkUtils.readInputData(inputStream);
            byte[] inputDataBytes = SymmetricEncryptionUtils.decrypt(inputDataBytesEncrypted, encryptionData);
            String inputData = new String(inputDataBytes);

            InvertedIndexResult invertedIndexResult = new Gson().fromJson(inputData, InvertedIndexResult.class);
            String status = invertedIndexResult.getStatus();

            if (!ResponseConstants.SUCCESS_REASON.equals(status)) {
                processFallback(invertedIndexResult, status);
            } else {
                String responseBody = invertedIndexResult.getResponseBody();
                rcpResponseFlowCommand.processCommand(FIND_WORD_OPERATION, responseBody);
            }

        } catch (Exception ex) {
            LOG.error("Exception during the processing: {}", ex.getMessage());
        }
    }

    private void processFallback(InvertedIndexResult invertedIndexResult, String status) {
        String responseBody = invertedIndexResult.getResponseBody();
        SingleMessage responseMessage = new Gson().fromJson(responseBody, SingleMessage.class);
        LOG.warn("'{}'. Message: {}", status, responseMessage);
    }

    private void doRequestDelay() {
        try {
            int delayValue = secureRandom.nextInt(maxDelay);
            LOG.info("Do delay '{}' seconds", delayValue);

            int requestTimeout = delayValue * 1000;
            Thread.sleep(requestTimeout);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void shutdown() {
        isEnabled.set(false);
    }
}
