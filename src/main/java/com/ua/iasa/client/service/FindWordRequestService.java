package com.ua.iasa.client.service;

import com.google.gson.Gson;
import com.ua.iasa.common.context.ClientConfigContext;
import com.ua.iasa.common.model.request.FindWordBody;
import com.ua.iasa.common.model.request.InvertedIndexCommand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static com.ua.iasa.common.constant.OperationConstants.FIND_WORD_OPERATION;

public class FindWordRequestService {

    private static FindWordRequestService instance;

    private final SecureRandom secureRandom = new SecureRandom();

    private final List<String> words;

    public FindWordRequestService(ClientConfigContext clientConfigContext) throws IOException {
        String wordsFilePath = clientConfigContext.getWordsFilePath();
        this.words = readWordsFromFile(wordsFilePath);
    }

    public InvertedIndexCommand createFindWordRequest() {
        String wordToFind = this.getRandomWord();
        FindWordBody findWordBody = new FindWordBody(wordToFind);
        String body = new Gson().toJson(findWordBody);
        return new InvertedIndexCommand(FIND_WORD_OPERATION, body);
    }

    public String getRandomWord() {
        int wordsCount = words.size();
        int wordIndex = secureRandom.nextInt(wordsCount);
        return words.get(wordIndex);
    }

    private static List<String> readWordsFromFile(String filePath) throws IOException {
        if (Objects.isNull(filePath) || filePath.isBlank() || filePath.isEmpty()) {
            throw new IOException();
        }

        File file = new File(filePath);

        if (!file.exists()) {
            throw new FileNotFoundException();
        }

        if (file.isDirectory()) {
            throw new IOException();
        }

        List<String> words = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String word = scanner.next();

                if (word.length() > 3 && !word.isBlank()) {
                    words.add(word);
                }
            }
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException();
        }

        return words;
    }

    public static FindWordRequestService getInstance() {
        return instance;
    }

    public static void create(ClientConfigContext clientConfigContext) throws IOException {
        if (instance == null) {
            instance = new FindWordRequestService(clientConfigContext);
        }
    }
}
