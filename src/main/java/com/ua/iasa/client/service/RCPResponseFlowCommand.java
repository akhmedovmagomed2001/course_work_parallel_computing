package com.ua.iasa.client.service;

import com.google.gson.Gson;
import com.ua.iasa.common.model.response.FindWordResponse;
import com.ua.iasa.common.model.response.SingleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import static com.ua.iasa.common.constant.OperationConstants.CLEAN_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.FIND_WORD_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_DIR_OPERATION;
import static com.ua.iasa.common.constant.OperationConstants.INDEX_FILE_OPERATION;

public class RCPResponseFlowCommand {

    private static final Logger LOG = LoggerFactory.getLogger(RCPResponseFlowCommand.class);

    private static RCPResponseFlowCommand instance;

    private final Map<String, Consumer<String>> flowMap = new HashMap<>();

    public RCPResponseFlowCommand() {
        flowMap.put(FIND_WORD_OPERATION, this::findFlow);
        flowMap.put(INDEX_DIR_OPERATION, this::singleMessageFlow);
        flowMap.put(INDEX_FILE_OPERATION, this::singleMessageFlow);
        flowMap.put(CLEAN_OPERATION, this::singleMessageFlow);
    }

    public void processCommand(String operation, String body) {
        if (!flowMap.containsKey(operation)) {
            String messagePattern = "Unknown operation: '%s'";
            String message = String.format(messagePattern, operation);
            LOG.error(message);
        } else {
            flowMap.get(operation).accept(body);
        }
    }

    private void singleMessageFlow(String body) {
        SingleMessage singleMessage = new Gson().fromJson(body, SingleMessage.class);
        String message = singleMessage.getMessage();
        LOG.info(message);
    }

    private void findFlow(String body) {
        FindWordResponse findWordResponse = new Gson().fromJson(body, FindWordResponse.class);
        String searchedWord = findWordResponse.getWord();
        Set<String> files = findWordResponse.getFiles();

        String message = String.format("Word '%s' is in files: %s", searchedWord, files);
        LOG.info(message);
    }

    public static RCPResponseFlowCommand getInstance() {

        if (instance == null) {
            instance = new RCPResponseFlowCommand();
        }

        return instance;
    }
}
