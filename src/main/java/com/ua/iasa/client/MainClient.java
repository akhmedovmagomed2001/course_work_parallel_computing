package com.ua.iasa.client;

import com.ua.iasa.client.service.FindWordRequestService;
import com.ua.iasa.client.thread.RPCClient;
import com.ua.iasa.common.context.ClientConfigContext;
import com.ua.iasa.common.thread.ShutdownRunnable;
import com.ua.iasa.common.thread.Timer;
import com.ua.iasa.common.util.ConfigUtils;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ua.iasa.common.constant.ConfigConstant.CONFIG_FILE_ENVIRONMENT_VARIABLE;

public class MainClient {

    public static void main(String[] args) throws IOException {
        Properties configs = ConfigUtils.parseConfigsByEnvironmentVariable(CONFIG_FILE_ENVIRONMENT_VARIABLE);
        ClientConfigContext clientConfigContext = new ClientConfigContext(configs);
        init(clientConfigContext);

        List<ShutdownRunnable> instances = Stream.generate(() -> new RPCClient(clientConfigContext))
                .limit(clientConfigContext.getThreadsCount())
                .collect(Collectors.toList());

        Timer timer = new Timer(instances, clientConfigContext.getWorkTime());

        Thread timerThread = new Thread(timer);
        timerThread.start();

        try {
            timerThread.join();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private static void init(ClientConfigContext clientConfigContext) throws IOException {
        FindWordRequestService.create(clientConfigContext);
    }
}
